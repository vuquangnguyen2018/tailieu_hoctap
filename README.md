VU QUANG NGUYEN 2018

01208611883
vuquangnguyen2016@gmail.com
facebook.com/vuquangnguyen2018a


![Brand_name_VQN_16.04.2018.png](https://bitbucket.org/repo/kMAa8Lr/images/2510284817-Brand_name_VQN_16.04.2018.png)
![21753039-603367200051267-6748639800835693957-o_orig.jpg](https://bitbucket.org/repo/kMAa8Lr/images/548595377-21753039-603367200051267-6748639800835693957-o_orig.jpg)
![Untitled-1.png](https://bitbucket.org/repo/kMAa8Lr/images/422903728-Untitled-1.png)

# Welcome


Welcome to your wiki! This is the default page we've installed for your convenience. Go ahead and edit it.

## Wiki features

This wiki uses the [Markdown](http://daringfireball.net/projects/markdown/) syntax. The [MarkDownDemo tutorial](https://bitbucket.org/tutorials/markdowndemo) shows how various elements are rendered. The [Bitbucket documentation](https://confluence.atlassian.com/x/FA4zDQ) has more information about using a wiki.

The wiki itself is actually a git repository, which means you can clone it, edit it locally/offline, add images or any other file type, and push it back to us. It will be live immediately.

Go ahead and try:

```
$ git clone https://vuquangnguyen2018@bitbucket.org/vuquangnguyen2018/webpage.git/wiki
```

Wiki pages are normal files, with the .md extension. You can edit them locally, as well as creating new ones.

## Syntax highlighting


You can also highlight snippets of text (we use the excellent [Pygments][] library).

[Pygments]: http://pygments.org/


Here's an example of some Python code:

```
#!python

def wiki_rocks(text):
    formatter = lambda t: "funky"+t
    return formatter(text)
```


You can check out the source of this page to see how that's done, and make sure to bookmark [the vast library of Pygment lexers][lexers], we accept the 'short name' or the 'mimetype' of anything in there.
[lexers]: http://pygments.org/docs/lexers/


Have fun!