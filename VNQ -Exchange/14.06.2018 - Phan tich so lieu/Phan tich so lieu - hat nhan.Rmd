---
title: "Phan tich so lieu hat nhan - R Notebook"
output:
  html_document: 
    df_print: default
    keep_md: yes
    toc: yes
  pdf_document: default
editor_options: 
  chunk_output_type: console
---
# VI DU  - trang 106

```{r}

## Bai vi du - 106 
x<-seq(0,100,20)
y<-c(-0.849,-0.196,0.734,1.541,2.456,3.318)
sigma_i<-rep(0.05,6)
f<-function(i){
  if (i==1) {rep(1,length(x))} # Ham lap lai rep(x,so lan lap lai)
  else {x}
}
beta_i<-function(i){
  return (sum(y*f(i)/sigma_i^2))
}
beta_matrix<-matrix(c(beta_i(1),beta_i(2)),nrow = 1,byrow = TRUE)
## Ma tran alpha

alpha_ij<-function(i,j){
  return (sum (f(i)*f(j)/sigma_i^2))
}

alpha_matrix<-matrix(c(alpha_ij(1,1),alpha_ij(1,2),alpha_ij(2,1),alpha_ij(2,2)),nrow=2,byrow = TRUE)
epsilon_matrix<-solve(alpha_matrix)
thamso_matrix<-beta_matrix %*% epsilon_matrix
saiso_matrix<-matrix(sqrt(c(epsilon_matrix[1,1],epsilon_matrix[2,2])),nrow =1,byrow=TRUE)



## Kiem dinh

kiemdinh<-function(alpha_kiemdinh){
  y_khop<-thamso_matrix[1,1]*f(1)+thamso_matrix[1,2]*f(2)
  Chisquare<-sum((y-y_khop)^2/sigma_i^2)
  
  ## Kiem dinh bang p-value
  p_value<-pchisq(Chisquare,df=length(x)-2,lower.tail = FALSE)
  if(p_value>alpha_kiemdinh/2 && p_value<(1-alpha_kiemdinh/2)){
    print("X ^2  and p -value")
    print(c(Chisquare,p_value))
    print("TRUE - Lam khop phu hop")
  }
  else{
    print("X ^2  and p -value")
     print(c(Chisquare,p_value))
    print("FALSE - Lam khop khong phu hop")
  }
}

alpha_matrix
beta_matrix
epsilon_matrix
thamso_matrix
saiso_matrix

kiemdinh(0.05)
y_khop<-thamso_matrix[1,1]*f(1)+thamso_matrix[1,2]*f(2)
data.frame(x,y,sigma_i,f(1),f(2),y_khop)

```


# Bai 6.2 trang 114

```{r}

## BAI 6.2 trang 114


library(matlib)
library(readxl)
bai_6.2 <- read_excel("B:/Workstation/Share_onday/14.06.2018 - Phan tich so lieu/On tap - Phan tich so lieu.xlsx", 
    sheet = "Bai 6.2")

x<-bai_6.2$x
y<-bai_6.2$y

sigma_i<-1.5
f<-function(i){
  if(i==1){return (rep(1,length(x)))}
  if (i==2){return (x) }
}
beta_i<-function(i){
  return (sum(y*f(i)/sigma_i^2))
}
beta_matrix<-matrix(c(beta_i(1),beta_i(2)),nrow = 1,byrow = TRUE)
## Ma tran alpha

alpha_ij<-function(i,j){
  return (sum (f(i)*f(j)/sigma_i^2))
}

alpha_matrix<-matrix(c(alpha_ij(1,1),alpha_ij(1,2),alpha_ij(2,1),alpha_ij(2,2)),nrow=2,byrow = TRUE)
epsilon_matrix<-solve(alpha_matrix)
thamso_matrix<-beta_matrix %*% epsilon_matrix
saiso_matrix<-matrix(sqrt(c(epsilon_matrix[1,1],epsilon_matrix[2,2])),nrow =1,byrow=TRUE)


kiemdinh<-function(alpha_kiemdinh){
  y_khop<-thamso_matrix[1,1]*f(1)+thamso_matrix[1,2]*f(2)
  Chisquare<-sum((y-y_khop)^2/sigma_i^2)
  p_value<-pchisq(Chisquare,df=length(x)-2,lower.tail = FALSE)
  if(p_value>alpha_kiemdinh/2 && p_value<(1-alpha_kiemdinh/2)){
    
    print("X ^2  and p -value")
    print(c(Chisquare,p_value))
    print("TRUE - Lam khop phu hop")
  }
  else{
    print("X ^2  and p -value")
     print(c(Chisquare,p_value))
    print("FALSE - Lam khop khong phu hop")
  }
}
alpha_matrix
beta_matrix
epsilon_matrix
thamso_matrix
saiso_matrix

kiemdinh(0.05)
y_khop<-thamso_matrix[1,1]*f(1)+thamso_matrix[1,2]*f(2)
data.frame(x,y,sigma_i,f(1),f(2),y_khop)




```





# BAI 6.3
```{r}

library(matlib)
library(readxl)
bai_6.3 <- read_excel("B:/Workstation/Share_onday/14.06.2018 - Phan tich so lieu/On tap - Phan tich so lieu.xlsx", 
    sheet = "Bai 6.3")
  
 

y<-bai_6.3$x+0.2 # delta(L) = mg/k nen y =delta(L)
x<-bai_6.3$y# m = x, y=a+bx
sigma_i<-rep(0.2,length(y))


f<-function(i){
  if (i==1){
    return(rep(1,length(x)))
  }
    else{
      return(x)
    }
}
  



beta_i<-function(i){
  ketqua<-sum(y*f(i)/sigma_i^2)
  return (ketqua)
}
beta_matrix<-matrix(c(beta_i(1),beta_i(2)),nrow = 1,byrow = TRUE)

alpha_ij<-function(i,j){
  ketqua<-sum (f(i)*f(j)/sigma_i^2)
    return (ketqua)
}

alpha_matrix<-matrix(c(alpha_ij(1,1),alpha_ij(1,2),alpha_ij(2,1),alpha_ij(2,2)),nrow=2,byrow = TRUE)
epsilon_matrix<-solve(alpha_matrix)
thamso_matrix<-beta_matrix %*% epsilon_matrix
saiso_matrix<-matrix(sqrt(c(epsilon_matrix[1,1],epsilon_matrix[2,2])),nrow =1,byrow=TRUE)



kiemdinh<-function(alpha_kiemdinh){
  y_khop<-thamso_matrix[1,1]*f(1)+thamso_matrix[1,2]*f(2)
  Chisquare<-sum((y-y_khop)^2/sigma_i^2)
  Chisquare
  p_value<-pchisq(Chisquare,df=length(x)-2,lower.tail = FALSE)
  p_value
  if(p_value>alpha_kiemdinh/2 && p_value<(1-alpha_kiemdinh/2)){
    
    print("X ^2  and p -value")
    print(c(Chisquare,p_value))
    print("TRUE - Lam khop phu hop")
  }
  else{
    print("X ^2  and p -value")
     print(c(Chisquare,p_value))
    print("FALSE - Lam khop khong phu hop")
  }
}
alpha_matrix
beta_matrix
epsilon_matrix
thamso_matrix
saiso_matrix

kiemdinh(0.05)
y_khop<-thamso_matrix[1,1]*f(1)+thamso_matrix[1,2]*f(2)
data.frame(x,y,sigma_i,f(1),f(2),y_khop)
```


# Bai 6.5

```{r}
library(matlib)
library(readxl)
Bai_6.5 <- read_excel("B:/Workstation/Share_onday/14.06.2018 - Phan tich so lieu/On tap - Phan tich so lieu.xlsx", 
    sheet = "Bai 6.5")
  
x<-Bai_6.5$x
y<-Bai_6.5$y
sigma_i<-sqrt(y)



f<-function(i){
  if(i==1){
    return ((1/pi) * (30/2) / ((x-102.1)^2+(30/2)^2) )
  } else{
    return ((1/pi) * (20/2) / ((x-177.9)^2+(20/2)^2) )
  }
}
beta_i<-function(i){
  ketqua<-sum(y*f(i)/sigma_i^2)
  return (ketqua)
}
beta_matrix<-matrix(c(beta_i(1),beta_i(2)),nrow = 1,byrow = TRUE)

alpha_ij<-function(i,j){
  ketqua<-sum (f(i)*f(j)/sigma_i^2)
  return (ketqua)
}

alpha_matrix<-matrix(c(alpha_ij(1,1),alpha_ij(1,2),alpha_ij(2,1),alpha_ij(2,2)),nrow=2,byrow = TRUE)
epsilon_matrix<-solve(alpha_matrix)
thamso_matrix<-beta_matrix %*% epsilon_matrix
saiso_matrix<-matrix(sqrt(c(epsilon_matrix[1,1],epsilon_matrix[2,2])),nrow =1,byrow=TRUE)
#kiem dinh
kiemdinh<-function(alpha_kiemdinh){
  y_khop<-thamso_matrix[1,1]*f(1)+thamso_matrix[1,2]*f(2)
  Chisquare<-sum((y-y_khop)^2/sigma_i^2)
  Chisquare
  p_value<-pchisq(Chisquare,df=length(x)-2,lower.tail = FALSE)
  p_value
  if(p_value>alpha_kiemdinh/2 && p_value<(1-alpha_kiemdinh/2)){
    
    print("X ^2  and p -value")
    print(c(Chisquare,p_value))
    print("TRUE - Lam khop phu hop")
  }
  else{
    print("X ^2  and p -value")
     print(c(Chisquare,p_value))
    print("FALSE - Lam khop khong phu hop")
  }
}
alpha_matrix
beta_matrix
epsilon_matrix
thamso_matrix
saiso_matrix
kiemdinh(0.05)
y_khop<-thamso_matrix[1,1]*f(1)+thamso_matrix[1,2]*f(2)
data.frame(x,y,sigma_i,f(1),f(2),y_khop)

```

# Bai 6.6

```{r}
library(matlib)
library(readxl)
Bai_6.6 <- read_excel("B:/Workstation/Share_onday/14.06.2018 - Phan tich so lieu/On tap - Phan tich so lieu.xlsx", 
    sheet = "Bai 6.6")
x<-Bai_6.6$x
y<-Bai_6.6$y
sigma_i<-sqrt((Bai_6.6$Ht)+(Bai_6.6$Hb))
# data.frame(x,y,Bai_6.6$Ht,Bai_6.6$Hb,sigma_i,f(1),f(2),f(3),y_khop)

f<-function(i){
  if(i==1){
    return (rep(1,length(x)))
    }
  else if(i==2){
    return (x)
    }
  else {
    return ((3*x^2-1)/2)
  } 
}

beta_i<-function(i){
  ketqua<-sum(y*f(i)/sigma_i^2)
  return(ketqua)
}
beta_matrix<-matrix(c(beta_i(1),beta_i(2),beta_i(3)),nrow = 1,byrow = TRUE)

alpha_ij<-function(i,j){
  ketqua=sum(f(i)*f(j)/sigma_i^2)
  return(ketqua)
}

alpha_matrix<-matrix(c(alpha_ij(1,1),alpha_ij(1,2),alpha_ij(1,3),
                       alpha_ij(2,1),alpha_ij(2,2),alpha_ij(2,3),
                       alpha_ij(3,1),alpha_ij(3,2),alpha_ij(3,3)),nrow=3,byrow = TRUE)
epsilon_matrix<-solve(alpha_matrix)
thamso_matrix<-beta_matrix %*% epsilon_matrix
saiso_matrix<-matrix(sqrt(c(epsilon_matrix[1,1],epsilon_matrix[2,2],epsilon_matrix[3,3])),nrow =1,byrow=TRUE)
#kiem dinh
kiemdinh<-function(alpha_kiemdinh){
  y_khop<-thamso_matrix[1,1]*f(1)+thamso_matrix[1,2]*f(2)+thamso_matrix[1,3]*f(3)
  Chisquare<-sum((y-y_khop)^2/sigma_i^2)
  p_value<-pchisq(Chisquare,df=length(x)-3,lower.tail = FALSE)
  if(p_value>alpha_kiemdinh/2 && p_value<(1-alpha_kiemdinh/2)){
    
    print("X ^2  and p -value")
    print(c(Chisquare,p_value))
    print("TRUE - Lam khop phu hop")
  }
  else{
    print("X ^2  and p -value")
     print(c(Chisquare,p_value))
    print("FALSE - Lam khop khong phu hop")
  }
}
alpha_matrix
beta_matrix
epsilon_matrix
thamso_matrix
saiso_matrix
kiemdinh(0.05)
 y_khop<-thamso_matrix[1,1]*f(1)+thamso_matrix[1,2]*f(2)+thamso_matrix[1,3]*f(3)
data.frame(x,y,sigma_i,f(1),f(2),f(3),y_khop)

```


# Bai 6.7

```{r}
library(matlib)
library(readxl)

Bai_6.7 <- read_excel("B:/Workstation/Share_onday/14.06.2018 - Phan tich so lieu/On tap - Phan tich so lieu.xlsx", 
    sheet = "Bai 6.7")

x<-Bai_6.7$x
y<-Bai_6.7$y
sigma_i<-rep(0.1,length(y))

f<-function(i){
  if(i==1){
    return(rep(1,length(x)))
    }
  else if(i==2){
    return(x)
    }
  else {
    return(x^2)
  } 
}

beta_i<-function(i){
  ketqua<-sum(y*f(i)/sigma_i^2)
  return(ketqua)
}
beta_matrix<-matrix(c(beta_i(1),beta_i(2),beta_i(3)),nrow = 1,byrow = TRUE)

alpha_ij<-function(i,j){
  ketqua<-sum(f(i)*f(j)/sigma_i^2)
  return(ketqua)
  }

alpha_matrix<-matrix(c(alpha_ij(1,1),alpha_ij(1,2),alpha_ij(1,3),
                       alpha_ij(2,1),alpha_ij(2,2),alpha_ij(2,3),
                       alpha_ij(3,1),alpha_ij(3,2),alpha_ij(3,3)),nrow=3,byrow = TRUE)
alpha_matrix
epsilon_matrix<-solve(alpha_matrix)
thamso_matrix<-beta_matrix %*% epsilon_matrix
saiso_matrix<-matrix(sqrt(c(epsilon_matrix[1,1],epsilon_matrix[2,2],epsilon_matrix[3,3])),nrow =1,byrow=TRUE)
#kiem dinh
kiemdinh<-function(alpha_kiemdinh){
  y_khop<-thamso_matrix[1,1]*f(1)+thamso_matrix[1,2]*f(2)+thamso_matrix[1,3]*f(3)
  Chisquare<-sum((y-y_khop)^2/sigma_i^2)
  p_value<-pchisq(Chisquare,df=length(x)-3,lower.tail = FALSE)
  if(p_value>alpha_kiemdinh/2 && p_value<(1-alpha_kiemdinh/2)){
    
    print("X ^2  and p -value")
    print(c(Chisquare,p_value))
    print("TRUE - Lam khop phu hop")
  }
  else{
    print("X ^2  and p -value")
     print(c(Chisquare,p_value))
    print("FALSE - Lam khop khong phu hop")
  }
}
alpha_matrix
beta_matrix
epsilon_matrix
thamso_matrix
saiso_matrix
kiemdinh(0.05)
 y_khop<-thamso_matrix[1,1]*f(1)+thamso_matrix[1,2]*f(2)+thamso_matrix[1,3]*f(3)
data.frame(x,y,sigma_i,f(1),f(2),f(3),y_khop)


```
# Bai 6.8


```{r}
library(matlib)
library(readxl)
Bai_6.8 <- read_excel("B:/Workstation/Share_onday/14.06.2018 - Phan tich so lieu/On tap - Phan tich so lieu.xlsx", 
    sheet = "Bai 6.8")
x<-Bai_6.8$x
y<-Bai_6.8$y
H<-Bai_6.8$H

sigma_i<-1/H*sqrt(H)

f<-function(i){
  if(i==1){
    return(rep(1,length(x)))
    }
  else if(i==2){
    return(x)
    }
  
}

beta_i<-function(i){
  ketqua<-sum(y*f(i)/sigma_i^2)
  return(ketqua)
}
beta_matrix<-matrix(c(beta_i(1),beta_i(2)),nrow = 1,byrow = TRUE)

alpha_ij<-function(i,j){
  ketqua<-sum(f(i)*f(j)/sigma_i^2)
  return(ketqua)
  }

alpha_matrix<-matrix(c(alpha_ij(1,1),alpha_ij(1,2),
                       alpha_ij(2,1),alpha_ij(2,2)),nrow=2,byrow = TRUE)

epsilon_matrix<-solve(alpha_matrix)
thamso_matrix<-beta_matrix %*% epsilon_matrix
saiso_matrix<-matrix(sqrt(c(epsilon_matrix[1,1],epsilon_matrix[2,2])),nrow =1,byrow=TRUE)

#kiem dinh
kiemdinh<-function(alpha_kiemdinh){
  y_khop<-thamso_matrix[1,1]*f(1)+thamso_matrix[1,2]*f(2)
  Chisquare<-sum((y-y_khop)^2/sigma_i^2)
  p_value<-pchisq(Chisquare,df=length(x)-2,lower.tail = FALSE)
  if(p_value>alpha_kiemdinh/2 && p_value<(1-alpha_kiemdinh/2)){
    
    print("X ^2  and p -value")
    print(c(Chisquare,p_value))
    print("TRUE - Lam khop phu hop")
  }
  else{
    print("X ^2  and p -value")
     print(c(Chisquare,p_value))
    print("FALSE - Lam khop khong phu hop")
  }
}
alpha_matrix
beta_matrix
epsilon_matrix
thamso_matrix
saiso_matrix
kiemdinh(0.05)
 y_khop<-thamso_matrix[1,1]*f(1)+thamso_matrix[1,2]*f(2)
data.frame(x,y,sigma_i,f(1),f(2),y_khop)

```

# V� d??? 7.2 (Phan tuong quan thong ke)

```{r}
library(readxl)
 Vidu7_2 <- read_excel("B:/Workstation/Share_onday/14.06.2018 - Phan tich so lieu/On tap - Phan tich so lieu.xlsx", 
    sheet = "VD 7.2")
x<-1/(Vidu7_2$x)^2*10^4
y<-Vidu7_2$y
sigma_i<-sqrt(y)
## he so tuong quan
data.frame(Vidu7_2$x,x,Vidu7_2$y,sigma_i^2,1/sigma_i^2,x/sigma_i^2,y/sigma_i^2,x^2/sigma_i^2,x*y/sigma_i^2)
r <-function(){
  tuso<-sum(1/sigma_i^2)*sum(x*y/sigma_i^2) - sum(x/sigma_i^2)*sum(y/sigma_i^2)
  mauso<-sqrt(sum(1/sigma_i^2)*sum(x^2/sigma_i^2)-sum(x/sigma_i^2)^2)*sqrt(sum(1/sigma_i^2)*sum(y^2/sigma_i^2)-sum(y/sigma_i^2)^2)
  return(tuso/mauso)
  
}

kiemdinh<-function(alpha_kiemdinh){
  t<-r()/sqrt((1-r()^2)/(length(x)-2))
  p_value<-pt(t,df=length(x)-2,lower.tail = FALSE)
  if(p_value>alpha_kiemdinh/2 && p_value<(1-alpha_kiemdinh/2)){
     print(t) 
      print("Gia tri p-value" )
       print(t,p_value)
            print("FALSE H0 \rho = 0 -> KHONG TUONG QUAN")
  }
  else{
    print(t)
    print("Gia tri p-value" )
    print(p_value)
    print("TRUE H1 \rho = 1 -> Tuong quan")
  }
}
r()
kiemdinh(0.05)
```


# BAI 7.4 
```{r}
library(readxl)
Bai_7.4  <- read_excel("B:/Workstation/Share_onday/14.06.2018 - Phan tich so lieu/On tap - Phan tich so lieu.xlsx", 
    sheet = "Bai 7.4")
mua<-Bai_7.4$Mua
Pu<-Bai_7.4$Pu
Sigma_Pu<-Bai_7.4$`Sigma Pu`
Cs<-Bai_7.4$Cs
Sigma_Cs<-Bai_7.4$`Sigma Cs`
Sigma_mua<-sqrt(mua)
# data.frame(mua,Pu,Sigma_Pu,Cs,Sigma_Cs)

## Buoc 1 : y(Pu) ~ x(Cs) = a + bx
x<-Cs
y<-Pu
sigma_i<-Sigma_Pu

f<-function(i){
  if (i==1){
    return(rep(1,length(x)))
  }
    else{
      return(x)
    }
}
beta_i<-function(i){
  ketqua<-sum(y*f(i)/sigma_i^2)
  return (ketqua)
}
beta_matrix<-matrix(c(beta_i(1),beta_i(2)),nrow = 1,byrow = TRUE)

alpha_ij<-function(i,j){
  ketqua<-sum (f(i)*f(j)/sigma_i^2)
    return (ketqua)
}

alpha_matrix<-matrix(c(alpha_ij(1,1),alpha_ij(1,2),alpha_ij(2,1),alpha_ij(2,2)),nrow=2,byrow = TRUE)
epsilon_matrix<-solve(alpha_matrix)
thamso_matrix<-beta_matrix %*% epsilon_matrix
b<-0.02128083
# > thamso_matrix
#             [,1]       [,2]
# [1,] 0.002270766 0.02128083
# > print(b)
# [1] 0.02128083



## Buoc 2 : y(Cs) ~ x(Pu) = a + bx
x<-Pu
y<-Cs
sigma_i<-Sigma_Cs

f<-function(i){
  if (i==1){
    return(rep(1,length(x)))
  }
    else{
      return(x)
    }
}
beta_i<-function(i){
  ketqua<-sum(y*f(i)/sigma_i^2)
  return (ketqua)
}
beta_matrix<-matrix(c(beta_i(1),beta_i(2)),nrow = 1,byrow = TRUE)

alpha_ij<-function(i,j){
  ketqua<-sum (f(i)*f(j)/sigma_i^2)
    return (ketqua)
}

alpha_matrix<-matrix(c(alpha_ij(1,1),alpha_ij(1,2),alpha_ij(2,1),alpha_ij(2,2)),nrow=2,byrow = TRUE)
epsilon_matrix<-solve(alpha_matrix)
thamso_matrix<-beta_matrix %*% epsilon_matrix
# > thamso_matrix
#           [,1]     [,2]
# [1,] 0.5117421 33.67815

b1<-33.67815

## Buoc 3 - kiem dinh su tuong quan

r<-sqrt(b*b1)

kiemdinh<-function(alpha_kiemdinh){
  t<-r/sqrt((1-r^2)/(length(Pu)-2))
  
   p_value<-pt(t,df=length(Pu)-2,lower.tail = FALSE)
  if(p_value>alpha_kiemdinh/2 && p_value<(1-alpha_kiemdinh/2)){
    
      print("Gia tri p-value" )
       print(p_value)
            print("FALSE H0 \rho = 0 -> KHONG TUONG QUAN")
  }
  else{
    print("Gia tri p-value" )
    print(p_value)
    print("TRUE H1 \rho = 1 -> Tuong quan")
  }
}

kiemdinh(0.05)




## Kiem dinh su tuong quang Pu va luong mua

## Buoc 1
## y (Pu) ~ x(Mua) = a + bx
x<-mua
y<-Pu
sigma_i<-Sigma_Pu

f<-function(i){
  if (i==1){
    return(rep(1,length(x)))
  }
    else{
      return(x)
    }
}
beta_i<-function(i){
  ketqua<-sum(y*f(i)/sigma_i^2)
  return (ketqua)
}
beta_matrix<-matrix(c(beta_i(1),beta_i(2)),nrow = 1,byrow = TRUE)

alpha_ij<-function(i,j){
  ketqua<-sum (f(i)*f(j)/sigma_i^2)
    return (ketqua)
}

alpha_matrix<-matrix(c(alpha_ij(1,1),alpha_ij(1,2),alpha_ij(2,1),alpha_ij(2,2)),nrow=2,byrow = TRUE)
epsilon_matrix<-solve(alpha_matrix)
thamso_matrix<-beta_matrix %*% epsilon_matrix
# > thamso_matrix
#             [,1]         [,2]
# [1,] -0.04818438 5.598952e-05


b<-5.598952e-05




## Buoc 2 : y(Mua) ~ x(Pu) = a + bx
x<-Pu
y<-mua
sigma_i<-Sigma_mua

f<-function(i){
  if (i==1){
    return(rep(1,length(x)))
  }
    else{
      return(x)
    }
}
beta_i<-function(i){
  ketqua<-sum(y*f(i)/sigma_i^2)
  return (ketqua)
}
beta_matrix<-matrix(c(beta_i(1),beta_i(2)),nrow = 1,byrow = TRUE)

alpha_ij<-function(i,j){
  ketqua<-sum (f(i)*f(j)/sigma_i^2)
    return (ketqua)
}

alpha_matrix<-matrix(c(alpha_ij(1,1),alpha_ij(1,2),alpha_ij(2,1),alpha_ij(2,2)),nrow=2,byrow = TRUE)
epsilon_matrix<-solve(alpha_matrix)
thamso_matrix<-beta_matrix %*% epsilon_matrix


# > thamso_matrix
#          [,1]     [,2]
# [1,] 1617.141 5259.272

b1<-5259.272


## Buoc 3 - kiem dinh su tuong quan

r<-sqrt(b*b1)

kiemdinh<-function(alpha_kiemdinh){
  t<-r/sqrt((1-r^2)/(length(Pu)-2))
  
   p_value<-pt(t,df=length(Pu)-2,lower.tail = FALSE)
  if(p_value>alpha_kiemdinh/2 && p_value<(1-alpha_kiemdinh/2)){
    
      print("Gia tri p-value" )
       print(p_value)
            print("FALSE H0 \rho = 0 -> KHONG TUONG QUAN")
  }
  else{
    print("Gia tri p-value" )
    print(p_value)
    print("TRUE H1 \rho = 1 -> Tuong quan")
  }
}

kiemdinh(0.05)
# [1] "Gia tri p-value"
# [1] 0.009986698
# [1] "TRUE H1 \rho = 1 -> Tuong quan"

```


# de thi 2017 - Giai de
```{r}
library(readxl)
Dethi2017 <- read_excel("B:/Workstation/Share_onday/14.06.2018 - Phan tich so lieu/On tap - Phan tich so lieu.xlsx", 
    sheet = "De thi 2017")
x<-Dethi2017$x
y<-Dethi2017$y
sigma_i<-Dethi2017$sigma_i

f<-function(i){
  if (i==1){
    return(1)
  }
  else{
    return(x)
  }
}

beta_i<-function(i){
  ketqua<-sum(y*f(i)/sigma_i^2)
  return (ketqua)
}

beta_matrix<-matrix(c(beta_i(1),beta_i(2)),nrow = 1,byrow = TRUE)

beta_matrix
alpha_ij<-function(i,j){
  ketqua<-sum (f(i)*f(j)/sigma_i^2)
    return (ketqua)
}

alpha_matrix<-matrix(c(alpha_ij(1,1),alpha_ij(1,2),alpha_ij(2,1),alpha_ij(2,2)),nrow=2,byrow = TRUE)
epsilon_matrix<-solve(alpha_matrix)
thamso_matrix<-beta_matrix %*% epsilon_matrix
saiso_matrix<-matrix(sqrt(c(epsilon_matrix[1,1],epsilon_matrix[2,2])),nrow=1,byrow = TRUE)

kiemdinh<-function(alpha_kiemdinh){
  y_khop<-thamso_matrix[1,1]*f(1)+thamso_matrix[1,2]*f(2)
  Chisquare<-sum((y-y_khop)^2/sigma_i^2)
  p_value<-pchisq(Chisquare,df=length(y)-2,lower.tail = FALSE)
  if(p_value>alpha_kiemdinh/2 && p_value <1-alpha_kiemdinh/2){
    print(p_value)
    
    return("TRUE - LAM KHOP PHU HOP")
  }else{
    print(p_value)
    return("FALSE - LAM KHOP KHONG PHU HOP")
  }
  
alpha_matrix
beta_matrix
epsilon_matrix
thamso_matrix
saiso_matrix
 y_khop<-thamso_matrix[1,1]*f(1)+thamso_matrix[1,2]*f(2)
data.frame(x,y,sigma_i,f(1),f(2),y_khop)
kiemdinh(0.05)
}


alpha_matrix
beta_matrix
epsilon_matrix
thamso_matrix
saiso_matrix

kiemdinh(0.05)

```

