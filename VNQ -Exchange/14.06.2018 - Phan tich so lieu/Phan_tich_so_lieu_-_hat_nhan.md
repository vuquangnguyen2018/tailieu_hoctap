---
title: "Phan tich so lieu hat nhan - R Notebook"
output:
  html_document: 
    df_print: default
    keep_md: yes
    toc: yes
  pdf_document: default
editor_options: 
  chunk_output_type: console
---
# VI DU  - trang 106


```r
## Bai vi du - 106 
x<-seq(0,100,20)
y<-c(-0.849,-0.196,0.734,1.541,2.456,3.318)
sigma_i<-rep(0.05,6)
f<-function(i){
  if (i==1) {rep(1,length(x))} # Ham lap lai rep(x,so lan lap lai)
  else {x}
}
beta_i<-function(i){
  return (sum(y*f(i)/sigma_i^2))
}
beta_matrix<-matrix(c(beta_i(1),beta_i(2)),nrow = 1,byrow = TRUE)
## Ma tran alpha

alpha_ij<-function(i,j){
  return (sum (f(i)*f(j)/sigma_i^2))
}

alpha_matrix<-matrix(c(alpha_ij(1,1),alpha_ij(1,2),alpha_ij(2,1),alpha_ij(2,2)),nrow=2,byrow = TRUE)
epsilon_matrix<-solve(alpha_matrix)
thamso_matrix<-beta_matrix %*% epsilon_matrix
saiso_matrix<-matrix(sqrt(c(epsilon_matrix[1,1],epsilon_matrix[2,2])),nrow =1,byrow=TRUE)



## Kiem dinh

kiemdinh<-function(alpha_kiemdinh){
  y_khop<-thamso_matrix[1,1]*f(1)+thamso_matrix[1,2]*f(2)
  Chisquare<-sum((y-y_khop)^2/sigma_i^2)
  
  ## Kiem dinh bang p-value
  p_value<-pchisq(Chisquare,df=length(x)-2,lower.tail = FALSE)
  if(p_value>alpha_kiemdinh/2 && p_value<(1-alpha_kiemdinh/2)){
    print("X ^2  and p -value")
    print(c(Chisquare,p_value))
    print("TRUE - Lam khop phu hop")
  }
  else{
    print("X ^2  and p -value")
     print(c(Chisquare,p_value))
    print("FALSE - Lam khop khong phu hop")
  }
}

alpha_matrix
```

```
##        [,1]    [,2]
## [1,]   2400  120000
## [2,] 120000 8800000
```

```r
beta_matrix
```

```
##        [,1]   [,2]
## [1,] 2801.6 258472
```

```r
epsilon_matrix
```

```
##               [,1]          [,2]
## [1,]  1.309524e-03 -1.785714e-05
## [2,] -1.785714e-05  3.571429e-07
```

```r
thamso_matrix
```

```
##            [,1]       [,2]
## [1,] -0.9468095 0.04228286
```

```r
saiso_matrix
```

```
##            [,1]         [,2]
## [1,] 0.03618734 0.0005976143
```

```r
kiemdinh(0.05)
```

```
## [1] "X ^2  and p -value"
## [1] 9.13251048 0.05787136
## [1] "TRUE - Lam khop phu hop"
```

```r
y_khop<-thamso_matrix[1,1]*f(1)+thamso_matrix[1,2]*f(2)
data.frame(x,y,sigma_i,f(1),f(2),y_khop)
```

```
##     x      y sigma_i f.1. f.2.     y_khop
## 1   0 -0.849    0.05    1    0 -0.9468095
## 2  20 -0.196    0.05    1   20 -0.1011524
## 3  40  0.734    0.05    1   40  0.7445048
## 4  60  1.541    0.05    1   60  1.5901619
## 5  80  2.456    0.05    1   80  2.4358190
## 6 100  3.318    0.05    1  100  3.2814762
```


# Bai 6.2 trang 114


```r
## BAI 6.2 trang 114


library(matlib)
library(readxl)
bai_6.2 <- read_excel("B:/Workstation/Share_onday/14.06.2018 - Phan tich so lieu/On tap - Phan tich so lieu.xlsx", 
    sheet = "Bai 6.2")

x<-bai_6.2$x
y<-bai_6.2$y

sigma_i<-1.5
f<-function(i){
  if(i==1){return (rep(1,length(x)))}
  if (i==2){return (x) }
}
beta_i<-function(i){
  return (sum(y*f(i)/sigma_i^2))
}
beta_matrix<-matrix(c(beta_i(1),beta_i(2)),nrow = 1,byrow = TRUE)
## Ma tran alpha

alpha_ij<-function(i,j){
  return (sum (f(i)*f(j)/sigma_i^2))
}

alpha_matrix<-matrix(c(alpha_ij(1,1),alpha_ij(1,2),alpha_ij(2,1),alpha_ij(2,2)),nrow=2,byrow = TRUE)
epsilon_matrix<-solve(alpha_matrix)
thamso_matrix<-beta_matrix %*% epsilon_matrix
saiso_matrix<-matrix(sqrt(c(epsilon_matrix[1,1],epsilon_matrix[2,2])),nrow =1,byrow=TRUE)


kiemdinh<-function(alpha_kiemdinh){
  y_khop<-thamso_matrix[1,1]*f(1)+thamso_matrix[1,2]*f(2)
  Chisquare<-sum((y-y_khop)^2/sigma_i^2)
  p_value<-pchisq(Chisquare,df=length(x)-2,lower.tail = FALSE)
  if(p_value>alpha_kiemdinh/2 && p_value<(1-alpha_kiemdinh/2)){
    
    print("X ^2  and p -value")
    print(c(Chisquare,p_value))
    print("TRUE - Lam khop phu hop")
  }
  else{
    print("X ^2  and p -value")
     print(c(Chisquare,p_value))
    print("FALSE - Lam khop khong phu hop")
  }
}
alpha_matrix
```

```
##           [,1]       [,2]
## [1,]  5.333333   69.33333
## [2,] 69.333333 1155.55556
```

```r
beta_matrix
```

```
##          [,1]     [,2]
## [1,] 248.3111 4161.956
```

```r
epsilon_matrix
```

```
##             [,1]         [,2]
## [1,]  0.85227273 -0.051136364
## [2,] -0.05113636  0.003933566
```

```r
thamso_matrix
```

```
##           [,1]     [,2]
## [1,] -1.198485 3.673601
```

```r
saiso_matrix
```

```
##           [,1]       [,2]
## [1,] 0.9231862 0.06271815
```

```r
kiemdinh(0.05)
```

```
## [1] "X ^2  and p -value"
## [1] 10.2269101  0.4208164
## [1] "TRUE - Lam khop phu hop"
```

```r
y_khop<-thamso_matrix[1,1]*f(1)+thamso_matrix[1,2]*f(2)
data.frame(x,y,sigma_i,f(1),f(2),y_khop)
```

```
##     x    y sigma_i f.1. f.2.    y_khop
## 1   2  5.3     1.5    1    2  6.148718
## 2   4 14.4     1.5    1    4 13.495921
## 3   6 20.7     1.5    1    6 20.843124
## 4   8 30.1     1.5    1    8 28.190326
## 5  10 35.0     1.5    1   10 35.537529
## 6  12 41.3     1.5    1   12 42.884732
## 7  14 52.7     1.5    1   14 50.231935
## 8  16 55.7     1.5    1   16 57.579138
## 9  18 63.0     1.5    1   18 64.926340
## 10 20 72.1     1.5    1   20 72.273543
## 11 22 80.5     1.5    1   22 79.620746
## 12 24 87.9     1.5    1   24 86.967949
```





# BAI 6.3

```r
library(matlib)
library(readxl)
bai_6.3 <- read_excel("B:/Workstation/Share_onday/14.06.2018 - Phan tich so lieu/On tap - Phan tich so lieu.xlsx", 
    sheet = "Bai 6.3")
  
 

y<-bai_6.3$x+0.2 # delta(L) = mg/k nen y =delta(L)
x<-bai_6.3$y# m = x, y=a+bx
sigma_i<-rep(0.2,length(y))


f<-function(i){
  if (i==1){
    return(rep(1,length(x)))
  }
    else{
      return(x)
    }
}
  



beta_i<-function(i){
  ketqua<-sum(y*f(i)/sigma_i^2)
  return (ketqua)
}
beta_matrix<-matrix(c(beta_i(1),beta_i(2)),nrow = 1,byrow = TRUE)

alpha_ij<-function(i,j){
  ketqua<-sum (f(i)*f(j)/sigma_i^2)
    return (ketqua)
}

alpha_matrix<-matrix(c(alpha_ij(1,1),alpha_ij(1,2),alpha_ij(2,1),alpha_ij(2,2)),nrow=2,byrow = TRUE)
epsilon_matrix<-solve(alpha_matrix)
thamso_matrix<-beta_matrix %*% epsilon_matrix
saiso_matrix<-matrix(sqrt(c(epsilon_matrix[1,1],epsilon_matrix[2,2])),nrow =1,byrow=TRUE)



kiemdinh<-function(alpha_kiemdinh){
  y_khop<-thamso_matrix[1,1]*f(1)+thamso_matrix[1,2]*f(2)
  Chisquare<-sum((y-y_khop)^2/sigma_i^2)
  Chisquare
  p_value<-pchisq(Chisquare,df=length(x)-2,lower.tail = FALSE)
  p_value
  if(p_value>alpha_kiemdinh/2 && p_value<(1-alpha_kiemdinh/2)){
    
    print("X ^2  and p -value")
    print(c(Chisquare,p_value))
    print("TRUE - Lam khop phu hop")
  }
  else{
    print("X ^2  and p -value")
     print(c(Chisquare,p_value))
    print("FALSE - Lam khop khong phu hop")
  }
}
alpha_matrix
```

```
##        [,1]    [,2]
## [1,]    200 1.1e+05
## [2,] 110000 7.1e+07
```

```r
beta_matrix
```

```
##      [,1]   [,2]
## [1,] 1405 836500
```

```r
epsilon_matrix
```

```
##               [,1]          [,2]
## [1,]  3.380952e-02 -5.238095e-05
## [2,] -5.238095e-05  9.523810e-08
```

```r
thamso_matrix
```

```
##          [,1]        [,2]
## [1,] 3.685714 0.006071429
```

```r
saiso_matrix
```

```
##           [,1]         [,2]
## [1,] 0.1838737 0.0003086067
```

```r
kiemdinh(0.05)
```

```
## [1] "X ^2  and p -value"
## [1] 8.8214286 0.1838727
## [1] "TRUE - Lam khop phu hop"
```

```r
y_khop<-thamso_matrix[1,1]*f(1)+thamso_matrix[1,2]*f(2)
data.frame(x,y,sigma_i,f(1),f(2),y_khop)
```

```
##     x   y sigma_i f.1. f.2.   y_khop
## 1 200 5.1     0.2    1  200 4.900000
## 2 300 5.5     0.2    1  300 5.507143
## 3 400 5.9     0.2    1  400 6.114286
## 4 500 6.8     0.2    1  500 6.721429
## 5 600 7.4     0.2    1  600 7.328571
## 6 700 7.5     0.2    1  700 7.935714
## 7 800 8.6     0.2    1  800 8.542857
## 8 900 9.4     0.2    1  900 9.150000
```


# Bai 6.5


```r
library(matlib)
library(readxl)
Bai_6.5 <- read_excel("B:/Workstation/Share_onday/14.06.2018 - Phan tich so lieu/On tap - Phan tich so lieu.xlsx", 
    sheet = "Bai 6.5")
  
x<-Bai_6.5$x
y<-Bai_6.5$y
sigma_i<-sqrt(y)



f<-function(i){
  if(i==1){
    return ((1/pi) * (30/2) / ((x-102.1)^2+(30/2)^2) )
  } else{
    return ((1/pi) * (20/2) / ((x-177.9)^2+(20/2)^2) )
  }
}
beta_i<-function(i){
  ketqua<-sum(y*f(i)/sigma_i^2)
  return (ketqua)
}
beta_matrix<-matrix(c(beta_i(1),beta_i(2)),nrow = 1,byrow = TRUE)

alpha_ij<-function(i,j){
  ketqua<-sum (f(i)*f(j)/sigma_i^2)
  return (ketqua)
}

alpha_matrix<-matrix(c(alpha_ij(1,1),alpha_ij(1,2),alpha_ij(2,1),alpha_ij(2,2)),nrow=2,byrow = TRUE)
epsilon_matrix<-solve(alpha_matrix)
thamso_matrix<-beta_matrix %*% epsilon_matrix
saiso_matrix<-matrix(sqrt(c(epsilon_matrix[1,1],epsilon_matrix[2,2])),nrow =1,byrow=TRUE)
#kiem dinh
kiemdinh<-function(alpha_kiemdinh){
  y_khop<-thamso_matrix[1,1]*f(1)+thamso_matrix[1,2]*f(2)
  Chisquare<-sum((y-y_khop)^2/sigma_i^2)
  Chisquare
  p_value<-pchisq(Chisquare,df=length(x)-2,lower.tail = FALSE)
  p_value
  if(p_value>alpha_kiemdinh/2 && p_value<(1-alpha_kiemdinh/2)){
    
    print("X ^2  and p -value")
    print(c(Chisquare,p_value))
    print("TRUE - Lam khop phu hop")
  }
  else{
    print("X ^2  and p -value")
     print(c(Chisquare,p_value))
    print("FALSE - Lam khop khong phu hop")
  }
}
alpha_matrix
```

```
##              [,1]         [,2]
## [1,] 4.082349e-05 4.966346e-06
## [2,] 4.966346e-06 2.984271e-05
```

```r
beta_matrix
```

```
##            [,1]       [,2]
## [1,] 0.08851768 0.09300197
```

```r
epsilon_matrix
```

```
##           [,1]      [,2]
## [1,] 25001.871 -4160.746
## [2,] -4160.746 34201.440
```

```r
thamso_matrix
```

```
##         [,1]     [,2]
## [1,] 1826.15 2812.502
```

```r
saiso_matrix
```

```
##          [,1]     [,2]
## [1,] 158.1198 184.9363
```

```r
kiemdinh(0.05)
```

```
## [1] "X ^2  and p -value"
## [1] 30.78522951  0.03047691
## [1] "TRUE - Lam khop phu hop"
```

```r
y_khop<-thamso_matrix[1,1]*f(1)+thamso_matrix[1,2]*f(2)
data.frame(x,y,sigma_i,f(1),f(2),y_khop)
```

```
##      x  y  sigma_i         f.1.         f.2.    y_khop
## 1   50  5 2.236068 0.0016243560 0.0001934026  3.510263
## 2   60  7 2.645751 0.0023904197 0.0002273575  5.004709
## 3   70 11 3.316625 0.0038032581 0.0002710771  7.707725
## 4   80 13 3.605551 0.0066927129 0.0003286828 13.146319
## 5   90 21 4.582576 0.0128554651 0.0004067125 24.619887
## 6  100 43 6.557439 0.0208127296 0.0005160323 39.458508
## 7  110 30 5.477226 0.0166126728 0.0006757583 32.237804
## 8  120 16 4.000000 0.0087542368 0.0009219933 18.579657
## 9  130 15 3.872983 0.0047584221 0.0013293876 12.428497
## 10 140 10 3.162278 0.0028738531 0.0020717770 11.074963
## 11 150 13 3.605551 0.0018951454 0.0036237052 13.652497
## 12 160 42 6.480741 0.0013346662 0.0075714157 23.731921
## 13 170 90 9.486833 0.0009874340 0.0195991556 56.925863
## 14 180 75 8.660254 0.0007586743 0.0304865325 87.128880
## 15 190 29 5.385165 0.0006004782 0.0129178964 37.428170
## 16 200 13 3.605551 0.0004867416 0.0054096614 16.103546
## 17 210  8 2.828427 0.0004023328 0.0028158800  8.654387
## 18 220  4 2.000000 0.0003380184 0.0017000010  5.398528
## 19 230  6 2.449490 0.0002879172 0.0011310004  3.706721
## 20 240  3 1.732051 0.0002481444 0.0008045422  2.715925
```

# Bai 6.6


```r
library(matlib)
library(readxl)
Bai_6.6 <- read_excel("B:/Workstation/Share_onday/14.06.2018 - Phan tich so lieu/On tap - Phan tich so lieu.xlsx", 
    sheet = "Bai 6.6")
x<-Bai_6.6$x
y<-Bai_6.6$y
sigma_i<-sqrt((Bai_6.6$Ht)+(Bai_6.6$Hb))
# data.frame(x,y,Bai_6.6$Ht,Bai_6.6$Hb,sigma_i,f(1),f(2),f(3),y_khop)

f<-function(i){
  if(i==1){
    return (rep(1,length(x)))
    }
  else if(i==2){
    return (x)
    }
  else {
    return ((3*x^2-1)/2)
  } 
}

beta_i<-function(i){
  ketqua<-sum(y*f(i)/sigma_i^2)
  return(ketqua)
}
beta_matrix<-matrix(c(beta_i(1),beta_i(2),beta_i(3)),nrow = 1,byrow = TRUE)

alpha_ij<-function(i,j){
  ketqua=sum(f(i)*f(j)/sigma_i^2)
  return(ketqua)
}

alpha_matrix<-matrix(c(alpha_ij(1,1),alpha_ij(1,2),alpha_ij(1,3),
                       alpha_ij(2,1),alpha_ij(2,2),alpha_ij(2,3),
                       alpha_ij(3,1),alpha_ij(3,2),alpha_ij(3,3)),nrow=3,byrow = TRUE)
epsilon_matrix<-solve(alpha_matrix)
thamso_matrix<-beta_matrix %*% epsilon_matrix
saiso_matrix<-matrix(sqrt(c(epsilon_matrix[1,1],epsilon_matrix[2,2],epsilon_matrix[3,3])),nrow =1,byrow=TRUE)
#kiem dinh
kiemdinh<-function(alpha_kiemdinh){
  y_khop<-thamso_matrix[1,1]*f(1)+thamso_matrix[1,2]*f(2)+thamso_matrix[1,3]*f(3)
  Chisquare<-sum((y-y_khop)^2/sigma_i^2)
  p_value<-pchisq(Chisquare,df=length(x)-3,lower.tail = FALSE)
  if(p_value>alpha_kiemdinh/2 && p_value<(1-alpha_kiemdinh/2)){
    
    print("X ^2  and p -value")
    print(c(Chisquare,p_value))
    print("TRUE - Lam khop phu hop")
  }
  else{
    print("X ^2  and p -value")
     print(c(Chisquare,p_value))
    print("FALSE - Lam khop khong phu hop")
  }
}
alpha_matrix
```

```
##              [,1]        [,2]         [,3]
## [1,]  0.114343160 0.009021607 -0.020763413
## [2,]  0.009021607 0.024272111  0.004970494
## [3,] -0.020763413 0.004970494  0.020466478
```

```r
beta_matrix
```

```
##          [,1]      [,2]       [,3]
## [1,] 9.143957 0.7957044 0.08653375
```

```r
epsilon_matrix
```

```
##          [,1]      [,2]      [,3]
## [1,] 11.80601  -7.19889  13.72562
## [2,] -7.19889  47.74541 -18.89880
## [3,] 13.72562 -18.89880  67.37491
```

```r
thamso_matrix
```

```
##          [,1]     [,2]     [,3]
## [1,] 103.4132 -29.4705 116.2988
```

```r
saiso_matrix
```

```
##          [,1]     [,2]     [,3]
## [1,] 3.435987 6.909805 8.208222
```

```r
kiemdinh(0.05)
```

```
## [1] "X ^2  and p -value"
## [1] 10.3315599  0.1705499
## [1] "TRUE - Lam khop phu hop"
```

```r
 y_khop<-thamso_matrix[1,1]*f(1)+thamso_matrix[1,2]*f(2)+thamso_matrix[1,3]*f(3)
data.frame(x,y,sigma_i,f(1),f(2),f(3),y_khop)
```

```
##       x   y   sigma_i f.1. f.2.  f.3.    y_khop
## 1  -0.8 179 13.747727    1 -0.8  0.46 180.48702
## 2  -0.6 124 11.489125    1 -0.6  0.04 125.74742
## 3  -0.4  95 10.148892    1 -0.4 -0.26  84.96368
## 4  -0.2  48  7.071068    1 -0.2 -0.44  58.13579
## 5   0.0  50  7.483315    1  0.0 -0.50  45.26377
## 6   0.2  54  7.483315    1  0.2 -0.44  46.34760
## 7   0.4  66  8.602325    1  0.4 -0.26  61.38728
## 8   0.6  72  9.486833    1  0.6  0.04  90.38283
## 9   0.8 128 12.000000    1  0.8  0.46 133.33423
## 10  1.0 209 14.933185    1  1.0  1.00 190.24149
```


# Bai 6.7


```r
library(matlib)
library(readxl)

Bai_6.7 <- read_excel("B:/Workstation/Share_onday/14.06.2018 - Phan tich so lieu/On tap - Phan tich so lieu.xlsx", 
    sheet = "Bai 6.7")

x<-Bai_6.7$x
y<-Bai_6.7$y
sigma_i<-rep(0.1,length(y))

f<-function(i){
  if(i==1){
    return(rep(1,length(x)))
    }
  else if(i==2){
    return(x)
    }
  else {
    return(x^2)
  } 
}

beta_i<-function(i){
  ketqua<-sum(y*f(i)/sigma_i^2)
  return(ketqua)
}
beta_matrix<-matrix(c(beta_i(1),beta_i(2),beta_i(3)),nrow = 1,byrow = TRUE)

alpha_ij<-function(i,j){
  ketqua<-sum(f(i)*f(j)/sigma_i^2)
  return(ketqua)
  }

alpha_matrix<-matrix(c(alpha_ij(1,1),alpha_ij(1,2),alpha_ij(1,3),
                       alpha_ij(2,1),alpha_ij(2,2),alpha_ij(2,3),
                       alpha_ij(3,1),alpha_ij(3,2),alpha_ij(3,3)),nrow=3,byrow = TRUE)
alpha_matrix
```

```
##           [,1]      [,2]      [,3]
## [1,] 1100.0000 246.00000 68.901800
## [2,]  246.0000  68.90180 20.802115
## [3,]   68.9018  20.80211  6.577481
```

```r
epsilon_matrix<-solve(alpha_matrix)
thamso_matrix<-beta_matrix %*% epsilon_matrix
saiso_matrix<-matrix(sqrt(c(epsilon_matrix[1,1],epsilon_matrix[2,2],epsilon_matrix[3,3])),nrow =1,byrow=TRUE)
#kiem dinh
kiemdinh<-function(alpha_kiemdinh){
  y_khop<-thamso_matrix[1,1]*f(1)+thamso_matrix[1,2]*f(2)+thamso_matrix[1,3]*f(3)
  Chisquare<-sum((y-y_khop)^2/sigma_i^2)
  p_value<-pchisq(Chisquare,df=length(x)-3,lower.tail = FALSE)
  if(p_value>alpha_kiemdinh/2 && p_value<(1-alpha_kiemdinh/2)){
    
    print("X ^2  and p -value")
    print(c(Chisquare,p_value))
    print("TRUE - Lam khop phu hop")
  }
  else{
    print("X ^2  and p -value")
     print(c(Chisquare,p_value))
    print("FALSE - Lam khop khong phu hop")
  }
}
alpha_matrix
```

```
##           [,1]      [,2]      [,3]
## [1,] 1100.0000 246.00000 68.901800
## [2,]  246.0000  68.90180 20.802115
## [3,]   68.9018  20.80211  6.577481
```

```r
beta_matrix
```

```
##      [,1]   [,2]     [,3]
## [1,] 5500 1613.7 501.6725
```

```r
epsilon_matrix
```

```
##              [,1]        [,2]      [,3]
## [1,]  0.008017553 -0.07235638  0.144849
## [2,] -0.072356375  0.97428511 -2.323337
## [3,]  0.144849046 -2.32333733  5.982529
```

```r
thamso_matrix
```

```
##             [,1]     [,2]    [,3]
## [1,] 0.001841868 8.689369 48.7707
```

```r
saiso_matrix
```

```
##            [,1]      [,2]     [,3]
## [1,] 0.08954079 0.9870588 2.445921
```

```r
kiemdinh(0.05)
```

```
## [1] "X ^2  and p -value"
## [1] 0.9149034 0.9987310
## [1] "FALSE - Lam khop khong phu hop"
```

```r
 y_khop<-thamso_matrix[1,1]*f(1)+thamso_matrix[1,2]*f(2)+thamso_matrix[1,3]*f(3)
data.frame(x,y,sigma_i,f(1),f(2),f(3),y_khop)
```

```
##        x  y sigma_i f.1.  f.2.     f.3.       y_khop
## 1  0.000  0     0.1    1 0.000 0.000000  0.001841868
## 2  0.079  1     0.1    1 0.079 0.006241  0.992679987
## 3  0.132  2     0.1    1 0.132 0.017424  1.998619311
## 4  0.174  3     0.1    1 0.174 0.030276  2.990373873
## 5  0.212  4     0.1    1 0.212 0.044944  4.035938552
## 6  0.244  5     0.1    1 0.244 0.059536  5.025660441
## 7  0.271  6     0.1    1 0.271 0.073441  5.938430012
## 8  0.301  7     0.1    1 0.301 0.090601  7.036016324
## 9  0.325  8     0.1    1 0.325 0.105625  7.977292202
## 10 0.349  9     0.1    1 0.349 0.121801  8.974751928
## 11 0.373 10     0.1    1 0.373 0.139129 10.028395502
```
# Bai 6.8



```r
library(matlib)
library(readxl)
Bai_6.8 <- read_excel("B:/Workstation/Share_onday/14.06.2018 - Phan tich so lieu/On tap - Phan tich so lieu.xlsx", 
    sheet = "Bai 6.8")
x<-Bai_6.8$x
y<-Bai_6.8$y
H<-Bai_6.8$H

sigma_i<-1/H*sqrt(H)

f<-function(i){
  if(i==1){
    return(rep(1,length(x)))
    }
  else if(i==2){
    return(x)
    }
  
}

beta_i<-function(i){
  ketqua<-sum(y*f(i)/sigma_i^2)
  return(ketqua)
}
beta_matrix<-matrix(c(beta_i(1),beta_i(2)),nrow = 1,byrow = TRUE)

alpha_ij<-function(i,j){
  ketqua<-sum(f(i)*f(j)/sigma_i^2)
  return(ketqua)
  }

alpha_matrix<-matrix(c(alpha_ij(1,1),alpha_ij(1,2),
                       alpha_ij(2,1),alpha_ij(2,2)),nrow=2,byrow = TRUE)

epsilon_matrix<-solve(alpha_matrix)
thamso_matrix<-beta_matrix %*% epsilon_matrix
saiso_matrix<-matrix(sqrt(c(epsilon_matrix[1,1],epsilon_matrix[2,2])),nrow =1,byrow=TRUE)

#kiem dinh
kiemdinh<-function(alpha_kiemdinh){
  y_khop<-thamso_matrix[1,1]*f(1)+thamso_matrix[1,2]*f(2)
  Chisquare<-sum((y-y_khop)^2/sigma_i^2)
  p_value<-pchisq(Chisquare,df=length(x)-2,lower.tail = FALSE)
  if(p_value>alpha_kiemdinh/2 && p_value<(1-alpha_kiemdinh/2)){
    
    print("X ^2  and p -value")
    print(c(Chisquare,p_value))
    print("TRUE - Lam khop phu hop")
  }
  else{
    print("X ^2  and p -value")
     print(c(Chisquare,p_value))
    print("FALSE - Lam khop khong phu hop")
  }
}
alpha_matrix
```

```
##          [,1]       [,2]
## [1,]   664.28   17295.48
## [2,] 17295.48 1220718.16
```

```r
beta_matrix
```

```
##          [,1]     [,2]
## [1,] 3558.894 71159.06
```

```r
epsilon_matrix
```

```
##               [,1]          [,2]
## [1,]  2.385310e-03 -3.379574e-05
## [2,] -3.379574e-05  1.298018e-06
```

```r
thamso_matrix
```

```
##          [,1]        [,2]
## [1,] 6.084192 -0.02790976
```

```r
saiso_matrix
```

```
##            [,1]        [,2]
## [1,] 0.04883963 0.001139306
```

```r
kiemdinh(0.05)
```

```
## [1] "X ^2  and p -value"
## [1] 0.01083613 0.99999997
## [1] "FALSE - Lam khop khong phu hop"
```

```r
 y_khop<-thamso_matrix[1,1]*f(1)+thamso_matrix[1,2]*f(2)
data.frame(x,y,sigma_i,f(1),f(2),y_khop)
```

```
##     x          y    sigma_i f.1. f.2.        y_khop
## 1   4 5.97177191 0.05049475    1    4  5.9725525872
## 2  36 5.08388576 0.07871332    1   36  5.0794402347
## 3  68 4.18205014 0.12356041    1   68  4.1863278823
## 4 100 3.28840189 0.19316685    1  100  3.2932155299
## 5 132 2.38876279 0.30289127    1  132  2.4001031774
## 6 164 1.51732262 0.46829291    1  164  1.5069908250
## 7 196 0.62057649 0.73323558    1  196  0.6138784726
## 8 218 0.05826891 0.97128586    1  218 -0.0001362697
```

# V� d??? 7.2 (Phan tuong quan thong ke)


```r
library(readxl)
 Vidu7_2 <- read_excel("B:/Workstation/Share_onday/14.06.2018 - Phan tich so lieu/On tap - Phan tich so lieu.xlsx", 
    sheet = "VD 7.2")
x<-1/(Vidu7_2$x)^2*10^4
y<-Vidu7_2$y
sigma_i<-sqrt(y)
## he so tuong quan
data.frame(Vidu7_2$x,x,Vidu7_2$y,sigma_i^2,1/sigma_i^2,x/sigma_i^2,y/sigma_i^2,x^2/sigma_i^2,x*y/sigma_i^2)
```

```
##    Vidu7_2.x         x Vidu7_2.y sigma_i.2 X1.sigma_i.2 x.sigma_i.2
## 1         20 25.000000       901       901  0.001109878 0.027746948
## 2         25 16.000000       652       652  0.001533742 0.024539877
## 3         30 11.111111       443       443  0.002257336 0.025081515
## 4         35  8.163265       339       339  0.002949853 0.024080429
## 5         40  6.250000       283       283  0.003533569 0.022084806
## 6         45  4.938272       281       281  0.003558719 0.017573920
## 7         50  4.000000       240       240  0.004166667 0.016666667
## 8         60  2.777778       220       220  0.004545455 0.012626263
## 9         75  1.777778       180       180  0.005555556 0.009876543
## 10       100  1.000000       154       154  0.006493506 0.006493506
##    y.sigma_i.2 x.2.sigma_i.2 x...y.sigma_i.2
## 1            1   0.693673696       25.000000
## 2            1   0.392638037       16.000000
## 3            1   0.278683499       11.111111
## 4            1   0.196574928        8.163265
## 5            1   0.138030035        6.250000
## 6            1   0.086784792        4.938272
## 7            1   0.066666667        4.000000
## 8            1   0.035072952        2.777778
## 9            1   0.017558299        1.777778
## 10           1   0.006493506        1.000000
```

```r
r <-function(){
  tuso<-sum(1/sigma_i^2)*sum(x*y/sigma_i^2) - sum(x/sigma_i^2)*sum(y/sigma_i^2)
  mauso<-sqrt(sum(1/sigma_i^2)*sum(x^2/sigma_i^2)-sum(x/sigma_i^2)^2)*sqrt(sum(1/sigma_i^2)*sum(y^2/sigma_i^2)-sum(y/sigma_i^2)^2)
  return(tuso/mauso)
  
}

kiemdinh<-function(alpha_kiemdinh){
  t<-r()/sqrt((1-r()^2)/(length(x)-2))
  p_value<-pt(t,df=length(x)-2,lower.tail = FALSE)
  if(p_value>alpha_kiemdinh/2 && p_value<(1-alpha_kiemdinh/2)){
     print(t) 
      print("Gia tri p-value" )
       print(t,p_value)
            print("FALSE H0 \rho = 0 -> KHONG TUONG QUAN")
  }
  else{
    print(t)
    print("Gia tri p-value" )
    print(p_value)
    print("TRUE H1 \rho = 1 -> Tuong quan")
  }
}
r()
```

```
## [1] 0.9938466
```

```r
kiemdinh(0.05)
```

```
## [1] 25.37827
## [1] "Gia tri p-value"
## [1] 3.113072e-09
## [1] "TRUE H1 \rho = 1 -> Tuong quan"
```


# BAI 7.4 

```r
library(readxl)
Bai_7.4  <- read_excel("B:/Workstation/Share_onday/14.06.2018 - Phan tich so lieu/On tap - Phan tich so lieu.xlsx", 
    sheet = "Bai 7.4")
mua<-Bai_7.4$Mua
Pu<-Bai_7.4$Pu
Sigma_Pu<-Bai_7.4$`Sigma Pu`
Cs<-Bai_7.4$Cs
Sigma_Cs<-Bai_7.4$`Sigma Cs`
Sigma_mua<-sqrt(mua)
# data.frame(mua,Pu,Sigma_Pu,Cs,Sigma_Cs)

## Buoc 1 : y(Pu) ~ x(Cs) = a + bx
x<-Cs
y<-Pu
sigma_i<-Sigma_Pu

f<-function(i){
  if (i==1){
    return(rep(1,length(x)))
  }
    else{
      return(x)
    }
}
beta_i<-function(i){
  ketqua<-sum(y*f(i)/sigma_i^2)
  return (ketqua)
}
beta_matrix<-matrix(c(beta_i(1),beta_i(2)),nrow = 1,byrow = TRUE)

alpha_ij<-function(i,j){
  ketqua<-sum (f(i)*f(j)/sigma_i^2)
    return (ketqua)
}

alpha_matrix<-matrix(c(alpha_ij(1,1),alpha_ij(1,2),alpha_ij(2,1),alpha_ij(2,2)),nrow=2,byrow = TRUE)
epsilon_matrix<-solve(alpha_matrix)
thamso_matrix<-beta_matrix %*% epsilon_matrix
b<-0.02128083
# > thamso_matrix
#             [,1]       [,2]
# [1,] 0.002270766 0.02128083
# > print(b)
# [1] 0.02128083



## Buoc 2 : y(Cs) ~ x(Pu) = a + bx
x<-Pu
y<-Cs
sigma_i<-Sigma_Cs

f<-function(i){
  if (i==1){
    return(rep(1,length(x)))
  }
    else{
      return(x)
    }
}
beta_i<-function(i){
  ketqua<-sum(y*f(i)/sigma_i^2)
  return (ketqua)
}
beta_matrix<-matrix(c(beta_i(1),beta_i(2)),nrow = 1,byrow = TRUE)

alpha_ij<-function(i,j){
  ketqua<-sum (f(i)*f(j)/sigma_i^2)
    return (ketqua)
}

alpha_matrix<-matrix(c(alpha_ij(1,1),alpha_ij(1,2),alpha_ij(2,1),alpha_ij(2,2)),nrow=2,byrow = TRUE)
epsilon_matrix<-solve(alpha_matrix)
thamso_matrix<-beta_matrix %*% epsilon_matrix
# > thamso_matrix
#           [,1]     [,2]
# [1,] 0.5117421 33.67815

b1<-33.67815

## Buoc 3 - kiem dinh su tuong quan

r<-sqrt(b*b1)

kiemdinh<-function(alpha_kiemdinh){
  t<-r/sqrt((1-r^2)/(length(Pu)-2))
  
   p_value<-pt(t,df=length(Pu)-2,lower.tail = FALSE)
  if(p_value>alpha_kiemdinh/2 && p_value<(1-alpha_kiemdinh/2)){
    
      print("Gia tri p-value" )
       print(p_value)
            print("FALSE H0 \rho = 0 -> KHONG TUONG QUAN")
  }
  else{
    print("Gia tri p-value" )
    print(p_value)
    print("TRUE H1 \rho = 1 -> Tuong quan")
  }
}

kiemdinh(0.05)
```

```
## [1] "Gia tri p-value"
## [1] 4.712734e-06
## [1] "TRUE H1 \rho = 1 -> Tuong quan"
```

```r
## Kiem dinh su tuong quang Pu va luong mua

## Buoc 1
## y (Pu) ~ x(Mua) = a + bx
x<-mua
y<-Pu
sigma_i<-Sigma_Pu

f<-function(i){
  if (i==1){
    return(rep(1,length(x)))
  }
    else{
      return(x)
    }
}
beta_i<-function(i){
  ketqua<-sum(y*f(i)/sigma_i^2)
  return (ketqua)
}
beta_matrix<-matrix(c(beta_i(1),beta_i(2)),nrow = 1,byrow = TRUE)

alpha_ij<-function(i,j){
  ketqua<-sum (f(i)*f(j)/sigma_i^2)
    return (ketqua)
}

alpha_matrix<-matrix(c(alpha_ij(1,1),alpha_ij(1,2),alpha_ij(2,1),alpha_ij(2,2)),nrow=2,byrow = TRUE)
epsilon_matrix<-solve(alpha_matrix)
thamso_matrix<-beta_matrix %*% epsilon_matrix
# > thamso_matrix
#             [,1]         [,2]
# [1,] -0.04818438 5.598952e-05


b<-5.598952e-05




## Buoc 2 : y(Mua) ~ x(Pu) = a + bx
x<-Pu
y<-mua
sigma_i<-Sigma_mua

f<-function(i){
  if (i==1){
    return(rep(1,length(x)))
  }
    else{
      return(x)
    }
}
beta_i<-function(i){
  ketqua<-sum(y*f(i)/sigma_i^2)
  return (ketqua)
}
beta_matrix<-matrix(c(beta_i(1),beta_i(2)),nrow = 1,byrow = TRUE)

alpha_ij<-function(i,j){
  ketqua<-sum (f(i)*f(j)/sigma_i^2)
    return (ketqua)
}

alpha_matrix<-matrix(c(alpha_ij(1,1),alpha_ij(1,2),alpha_ij(2,1),alpha_ij(2,2)),nrow=2,byrow = TRUE)
epsilon_matrix<-solve(alpha_matrix)
thamso_matrix<-beta_matrix %*% epsilon_matrix


# > thamso_matrix
#          [,1]     [,2]
# [1,] 1617.141 5259.272

b1<-5259.272


## Buoc 3 - kiem dinh su tuong quan

r<-sqrt(b*b1)

kiemdinh<-function(alpha_kiemdinh){
  t<-r/sqrt((1-r^2)/(length(Pu)-2))
  
   p_value<-pt(t,df=length(Pu)-2,lower.tail = FALSE)
  if(p_value>alpha_kiemdinh/2 && p_value<(1-alpha_kiemdinh/2)){
    
      print("Gia tri p-value" )
       print(p_value)
            print("FALSE H0 \rho = 0 -> KHONG TUONG QUAN")
  }
  else{
    print("Gia tri p-value" )
    print(p_value)
    print("TRUE H1 \rho = 1 -> Tuong quan")
  }
}

kiemdinh(0.05)
```

```
## [1] "Gia tri p-value"
## [1] 0.009986698
## [1] "TRUE H1 \rho = 1 -> Tuong quan"
```

```r
# [1] "Gia tri p-value"
# [1] 0.009986698
# [1] "TRUE H1 \rho = 1 -> Tuong quan"
```


# de thi 2017 - Giai de

```r
library(readxl)
Dethi2017 <- read_excel("B:/Workstation/Share_onday/14.06.2018 - Phan tich so lieu/On tap - Phan tich so lieu.xlsx", 
    sheet = "De thi 2017")
x<-Dethi2017$x
y<-Dethi2017$y
sigma_i<-Dethi2017$sigma_i
data.frame(x,y,sigma_i)
```

```
##     x    y sigma_i
## 1   1  321    0.05
## 2   2  668    0.05
## 3   3  949    0.05
## 4   4 1138    0.05
## 5   5 1231    0.05
## 6   6 2429    0.05
## 7   7 2702    0.05
## 8   8 3121    0.05
## 9   9 3958    0.05
## 10 10 3294    0.05
## 11 11 3744    0.05
```

```r
f<-function(i){
  if (i==1){
    return(1)
  }
  else{
    return(x)
  }
}

beta_i<-function(i){
  ketqua<-sum(y*f(i)/sigma_i^2)
  return (ketqua)
}
beta_matrix<-matrix(c(beta_i(1),beta_i(2)),nrow = 1,byrow = TRUE)

alpha_ij<-function(i,j){
  ketqua<-sum (f(i)*f(j)/sigma_i^2)
    return (ketqua)
}

alpha_matrix<-matrix(c(alpha_ij(1,1),alpha_ij(1,2),alpha_ij(2,1),alpha_ij(2,2)),nrow=2,byrow = TRUE)
epsilon_matrix<-solve(alpha_matrix)
thamso_matrix<-beta_matrix %*% epsilon_matrix
saiso_matrix<-matrix(sqrt(c(epsilon_matrix[1,1],epsilon_matrix[2,2])),nrow=1,byrow = TRUE)

kiemdinh<-function(alpha_kiemdinh){
  y_khop<-thamso_matrix[1,1]*f(1)+thamso_matrix[1,2]*f(2)
  Chisquare<-sum((y-y_khop)^2/sigma_i^2)
  p_value<-pchisq(Chisquare,df=length(y)-2,lower.tail = FALSE)
  if(p_value>alpha_kiemdinh/2 && p_value <1-alpha_kiemdinh/2){
    print(p_value)
    
    return("TRUE - LAM KHOP PHU HOP")
  }else{
    print(p_value)
    return("FALSE - LAM KHOP KHONG PHU HOP")
  }
  
alpha_matrix
beta_matrix
epsilon_matrix
thamso_matrix
saiso_matrix
 y_khop<-thamso_matrix[1,1]*f(1)+thamso_matrix[1,2]*f(2)
data.frame(x,y,sigma_i,f(1),f(2),y_khop)
kiemdinh(0.05)
}


alpha_matrix
```

```
##       [,1]   [,2]
## [1,]  4400  26400
## [2,] 26400 202400
```

```r
beta_matrix
```

```
##         [,1]     [,2]
## [1,] 9422000 73365200
```

```r
epsilon_matrix
```

```
##               [,1]          [,2]
## [1,]  0.0010454545 -1.363636e-04
## [2,] -0.0001363636  2.272727e-05
```

```r
thamso_matrix
```

```
##           [,1]     [,2]
## [1,] -154.0727 382.5727
```

```r
saiso_matrix
```

```
##            [,1]        [,2]
## [1,] 0.03233349 0.004767313
```

```r
kiemdinh(0.05)
```

```
## [1] 0
```

```
## [1] "FALSE - LAM KHOP KHONG PHU HOP"
```

