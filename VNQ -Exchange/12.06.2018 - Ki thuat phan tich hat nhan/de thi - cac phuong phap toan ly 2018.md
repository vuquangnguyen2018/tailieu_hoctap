```tex
\documentclass[12pt]{article}
\usepackage[utf8]{vietnam}
\usepackage{amsfonts,amsmath}
\usepackage[margin=0.5in]{geometry}
\usepackage{bookmark}
\usepackage{physics}
\title{Đề thi cuối kì \\
CÁC PHƯƠNG PHÁP TOÁN LÝ}
\author{Học kì 2 - 2018}
\begin{document}\maketitle


\section*{Câu 1 - 0.25 điểm}

Để thiết lập bài toán giá trị đầu ta cần những gì? Kể ra.
\section*{Câu 2  - 2.75 điểm}

Dùng phương pháp tách biến giải phương trình thế\\
 	\begin{align}
 		\pdv[2]{u}{x} + \pdv[2]{u}{y} &= 0, \quad 0<x<1, 0<y<1\\
 		u(x = 0 ,y) &=0, \quad u(x=1,y) =0 \\
 		\pdv{u}{y} \qty(x,y=0) &= 0, \quad  \pdv{u}{y} \qty(x,y=1)=x(1-x^2) 
 	\end{align}
 \section*{Câu 3}
 
 Tính các tích phân sau:
 \begin{enumerate}
 	\item $\int \limits _0^\infty \sqrt{y} e^{-y^3}\,dy$
 	\item $ \int  \limits _0^\infty \cos(y) \delta (y+\pi) \,dy$
 	\item $ \int \limits _{-2}^0 e^x \sin \dfrac{\pi x }{2} \delta \qty(x^2-1)\,dx$\\
 	
 	Biết rằng,  $ \Gamma (n) = \int \limits _0^\infty x^{n-1} e^{-x} \,dx ; \quad \Gamma \qty[\dfrac{1}{2}] = \sqrt{\pi} $
 \end{enumerate}
 \section*{Câu 4}
 Tìm đường cong C có chiều dài l cho trước bao quanh một vùng diện tích cực đại. Biết diện tích giới hạn đường cong C là $A = \dfrac{1}{2} \oint_{(C)} \qty (x\,dy -y\,dx )$ còn độ dài đường cong $l = \oint_{C} \sqrt{\,dx^2 +\,dy^2}$
 \section*{Câu 5}
 Hàm sinh của đa thức Lagendre được cho như sau:
 \begin{align*}
  	\Phi \qty(x,h) =\dfrac{1}{\sqrt{1-2xh +h^2}}=\sum \limits _{n=0}^\infty P_n (x)h^n
 \end{align*}
 \begin{enumerate}
 	\item Chứng tỏ rằng hàm sinh này thoả mãn phương trình 
 	\begin{align*}
 		(1-2xh+h^2) \pdv{\Phi}{h}=(x-h)\Phi
 	\end{align*}
 	\item Dùng kết quả câu a dẫn ra hệ thức truy chứng $P_n $ , $P_{n-1} $ và $P_{n-2}$\\
 	\item Cho $P_0  =1$ , $P_1 =x$, tìm $P_2$ và $P_3$
 \end{enumerate}
\end{document}
```
