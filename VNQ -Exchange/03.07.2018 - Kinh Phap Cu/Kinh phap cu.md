## Phẩm ngu si

Mất ngủ thấy đêm dài <br />
Mệt nhoài thấy đường xa <br />
Kẻ ngu luân hồi mãi <br /> 
Chánh Pháp biết đâu là <br />
***

Nếu tìm không gặp bạn <br />
Hơn mình hay ngang mình <br />
Thà quyết sống một mình <br />
Chơ thân cận kẻ ngu <br />
***

Con ta tài sản ta  <br />
Kẻ ngu mãi lo xa <br />
Chính ta còn không có <br />
Tài sản con đâu ra <br />
*** 

Người ngu biết mình ngu <br />
Nhờ vậy thành có Trí <br />
Người ngu cho mình trí <br />
Thật đáng gọi chí ngu <br />

***
Kẻ ngu dầu trọn đời  <br />
Thân cận với người Trí <br />
Cũng không hiểu Pháp vị <br />
Như muỗng trong nồi canh <br />
***

Người Trí với người Trí <br />
Gần nhau trong phút giây <br />
Chánh Pháp nhận ra ngay <br />
Như lưỡi nếm canh vậy <br /> 
***

Kẻ ngu si thiếu trí <br /> 
Tự ngã hoá ra thù <br />
Ác nghiệp tạo lần hồi <br />
Phải chịu quả cay đắng <br />
***

Người tạo nghiệp bất thiện <br />
Làm xong sanh ăn năn <br />
Mắt đẫm lệ than rằng <br />
Phải chịu quả cay đắng <br />

***

Người tạo được thiện nghiệp <br />
Làm xong không ăn năn <br />
Hoan Hỷ lòng phơi phới <br />
Hái quả Phúc thường hằng <br />

***
Ác nghiệp chưa chín muồi <br />
Kẻ ngu tưởng đường mật <br />
Ác nghiệp khi chín thật <br />
Kẻ ngu gánh khổ đau <br />
 
***

Kẻ ngu sống thường hằng <br />
Nhờ ngọn cỏ Ku xa <br />
Chưa bằng phần mười sáu <br />
Người hiểu Chánh Pháp mà <br />

***
Ác nghiệp chưa kết trái <br />
Như sữa chưa đông ngay <br />
Nung đốt kẻ ngu này <br />
Tựa lửa phủ tro vậy

***
Kiến thức và Danh vọng <br />
Trở lại hại kẻ ngu <br />
Tiêu diệt ngay vận tốt <br />
Bửa nốt cả đầu ngu <br />

***
Kẻ ngu ham danh hảo <br />
Khoái ngồi trước Sa môn <br />
Ưa quyền trong tu viện <br />
Thích mọi người suy tôn <br />

***

Hãy để cả Tăng Tục <br />
Cho rằng việc ta làm <br />
Mặc dù lớn hay nhỏ <br />
Đều phải theo ý ta <br />
Kẻ ngu ôm khát vọng<br />
Dục mãn lớn dần ra<br />

***

Đường này đến thế gian <br />
Đường kia đến Niết bàn<br />
Tỳ kheo đệ tử Phật<br />
Phải ý thức rõ ràng<br />
Đừng đắm say Thế Lợi<br />
Hãy tu hạnh Ly Tham

***

## Phẩm Hiền trí

Nếu gặp bậc Hiền trí <br />
Chỉ trách điều lỗi lầm<br />
Hay tha thiết Kết thân<br />
Như người chỉ Kho báu<br />
Kết thân người như vây<br />
Không xấu tốt hơn nhiều<br />
***
Những ai thường khuyên dạy<br />
Ngăn chặn Tội ác sanh<br />
Được người hiền tán thành<br />
Bị kẻ ác ghét bỏ<br />
***
Chớ thân bạn xấu ác<br />
Chớ thân kẻ đê hèn<br />
Hãy thân bạn hiền lạnh<br />
Hãy thân người cao thượng<br />
***
Ai thấm nhuần Chánh pháp<br />
Người lấy tâm an bình<br />
Bậc trí vui chánh pháp<br />
Do thánh nhơn thuyết minh<br />

***

Người đem nước dẫn nước<br />
Tay làm tên vót tên<br />
Thợ mộc uốn gỗ bền<br />
Bậc Trí tự điều phục<br />
***
Như tảng đá kiên cố<br />
Không gió nào chuyển lay<br />
Bậc trí cũng thế này<br />
Khen chê chẳng dao dộng<br />
***
Như hồ nước sâu thẳm<br />
Yên lặng và trong xanh<br />
Bậc Trí nghe giáo pháp<br />
Tâm thanh tịnh an lành<br />
***
Hiển giả bỏ tất cả<br />
Thánh giả xả ái dục<br />
Khổ đau hay hạnh phúc<br />
Trí giả chả mừng lo<br />
***
Đừng vì mình vì người<br />
Làm điều gì sai trái<br />
Đừng mong cầu con cái<br />
Tài sản hay đất đai<br />
Bằng hành động lầm sai<br />
Thành công do bất chánh<br />
Người ấy thật đức hạnh<br />
Trí tuệ và chân thành<br />
***
Ít ngưười giữa nhân loại <br />
Đến được bờ bên kia<br />
Bao nhiêu người còn lại<br />
Quanh bờ bên này kìa<br />

***
Ai tu tập đúng pháp<br />
Được thuyết giảng rõ ràng<br />
Sẽ đạt đến niết bàn<br />
Vượt cõi dục khó vượt<br />
***
Người Trí bỏ pháp đen<br />
Tu tập Pháp trẳng cả<br />
Từ giã nhà xuất gia<br />
Vui viễn ly tịch tịnh<br />
***
Từ bỏ mọi dục lạc<br />
Giải thoát hết chướng phiền<br />
Người Trí nên trước tiên<br />
Thanh lọc tâm ô nhiễm<br />
***
Ai chánh tâm tu tập<br />
Hành pháp Thất giác tri<br />
Từ bỏ tâm ái nhiễm<br />
Vui đoạn Tánh chất trì<br />
Sẽ thanh tịnh sáng chói<br />
Đạt niết bàn đời nay<br />

### Phẩm A La Hán

Ai đi đường đến đích <br />
Diệt trừ hết ưu sầu<br />
Giải thoát mọi ràng buộc<br />
Tham dục chẳng còn đâu<br />
***
Ai nỗ lực Chánh niệm<br />
Không lưu luyến nơi nào<br />
Như ngỗng trời rời ao<br />
Từ bỏ mọi chỗ náu<br />

***
Ai từ bỏ tích luỹ<br />
Quán tưởng khi uống ăn<br />
Không vô tướng giải thoát<br />
Theo hương đó tu hành<br />
Như giữa trời chim lượn<br />
TÌm đâu ra mối manh<br />
***
Ai dứt được lậu hoặc<br />
Ăn uống chẳng tham tranh<br />
Không vô tương giải thoát<br />
Theo hướng đó tu hành<br />
Như giữa trời chim lượn<br />
Tìm đâu ra mối manh<br />
***
Ai nhiếp hộ các căn<br />
Như chiền mã thuần thục<br />
Mạn trừ lậu hoặc dứt<br />
Chư thiên cũng kính phục<br />
***
Như đất không hiền hận<br />
Như trụ chấn kiên trì<br />
Như hồ không vẫn đục<br />
Luân hồi hết chuyển di<br />

***
Người tâm thường an tịnh<br />
Ngôn hành đều tịnh an<br />
Chánh trí chơn giải thoát<br />
An tịnh thế hoàn toàn<br />
***
Không tin tưởng người khác<br />
Thông đạt lý vô sanh<br />
Cắt đứt mọi hệ luỵ<br />
Triệt tiêu các mối manh<br />
Tận diệt mọi tham ái<br />
Bậc thượng sĩ tu hành<br />
***

Làng mạc hay núi rừng<br />
Thung lũng hay đồi cao<br />
La Hán trú chỗ nào<br />
Nơi ấy được an lạc<br />
***
Phàm phu không ưa thích<br />
An trú giữa núi rừng<br />
Bậc ly tham vui mừng<br />
Vì không tìm dục lạc<br />
***
### Phẩm muôn ngàn
***
Chỉ một lời lợi ích<br />
Nghe xong Tâm Bình an<br />
Hơn tụng cả muôn ngàn <br />
Lời lời đều vô dụng<br />
***
Chỉ một câu Hữu ích<br />
Nghe xong Tâm bình an<br />
Hơn tụng ca muôn ngàn<br />
Những lời kệ vô dụng<br />
***

Chỉ một lời Chánh pháp<br />
Nghe xong tâm bình an<br />
Hơn tụng cả muôn ngàn<br />
Những lời kệ vô dụng<br />
***
Dầu tại bãi chiến trường<br />
Thắng ngàn ngàn quân địch<br />
Không bằng tự thắng mình<br />
Chiến công ấy kì tích<br />
***
Tự thắng mình vẻ vang<br />
Hơn chinh phục kẻ khác<br />
Người điều phục được mình<br />
Thường tự chế an lạc<br />
***
Thiên thần càn thác bà<br />
Ma Vương hay Phạm thiên<br />
Không thể chinh phục nỗi<br />
Người điều phục tự thân<br />
***
Tháng tháng cúng ngàn vàng<br />
Tế tự cả trăm năm<br />
Không bằng trong giây lát<br />
Kính lễ bậc Thánh tăng<br />
Kính lễ ấy hơn rằng<br />
Tế tự hằng thế kỉ<br />
***
  
Dầu trăm năm thành kính<br />
Thờ lửa tại rừng xanh<br />
Không bằng trong giây lát<br />
Kính lễ bậc thánh tăng<br />
Kính lễ vậy hơn rằng<br />
Bái lửa hàng thế kỉ<br />
***
  
Cầu phước suốt cả năm<br />
Cúng dường hoặc bố thí<br />
Không bằng một phần tư<br />
Kính lễ bậc Chánh Trí<br />
***
  
Thương tôn trọng kính lễ<br />
Bậc trưởng lão Cao minh<br />
Bốn phước lành tăng tưởng<br />
Thọ, vui, khoẻ, đẹp xinh<br />
***
  
Dù sống đến trăm năm<br />
Phá giới không an tịnh<br />
Chẳng bằng sống một ngày<br />
Trì giới tu thiền định<br />
***
  
Dù sống đến trăm năm<br />
Không Trí tuệ an tịnh<br />
Chẳng bằng sống một ngày<br />
Có Trí tuệ thiền định<br />
***
  
Dù sống đến trăm năm<br />
Lười biến không tinh tấn<br />
Chẳng bằng sống một ngày<br />
Nỗ lực tu tinh tấn<br />
***
  
Dù sống đến trăm năm <br />
Không thấp pháp Sanh diệt<br />
Chẳng bằng sống một ngày<br />
Thấy được pháp Sanh diệt<br />

***
  
   
Dù sống đến trăm năm<br />
Không thấy pháp Bất diệt<br />
Chẳng bằng sống một ngày<br />
Thấy được pháp Bất diệt<br />
***
  
   
Dù sống đến trăm năm<br />
Không thấy được phấp Tối thượng<br />
Chẳng bằng sống một ngày<br />
Thấy được pháp Tối thượng<br />
***
  
   
### Phẩm ác hạnh

Hãy gấp rút làm lành<br />
Đừng để tâm ác sanh<br />
Ai chậm làm việc thiện<br />
Ác nghiệp sẽ lộng hành<br />
***
  
   
Đã phạm phải điều ác<br />
Chớ tiếp tục phạm hoài<br />
Đừng ham làm việc ác<br />
Tích ác khổ dằng dai<br />
***
  
   
Đã làm được việc thiện<br />
Nên tiếp tục làm thêm<br />
Hãy vui làm việc thiện<br />
Tích thiện sống êm đềm<br />
***
  
   
Kẻ ác lại gặp may <br />
Khi ác chưa thật chín<br />
Một khi trái ác chín <br />
Kẻ ác gặp ác ngay<br />
***
  
   
Người thiện lại gặp ác<br />
Khi thiện chưa chín muồi<br />
Khi trái thiện chín muồi<br />
Người thiện lại gặp Phúc<br />
***
  
   
Chớ khinh thường việc thiện<br />
Cho rằng chưa đến ta<br />
Như nước nhỏ từng giọt<br />
Từ tư bình tràn ra<br />
Người Trí được toàn thiện<br />
Do tích luỹ dần dà<br />
***
  
   
Chớ khinh thường điều ác<br />
Cho rằng chưa đến ta<br />
Như nước nhỏ từng giọt<br />
Từ từ bình tràn ra<br />
Kẻ ngu đầy tội ác<br />
Do chất chứa dần dà<br />
***
  
   
Như thương gia nhiều của<br />
Ít bạn tránh đường nguy<br />
Muốn tránh chất độc hại<br />
Ác nghiệp phải tránh đi<br />
***
  
   
Bàn tay không thương tích<br />
Cầm thuốc độc không sao<br />
Người không làm việc ác<br />
Không bị ác nhiễm vào<br />
***
  
   
Hại người không ác tâm<br />
Thanh tịnh và vô tội<br />
Tội ác dội kẻ ngu<br />
Như ngược gió tung bụi<br />
***
  
   
Một số sanh bào thai<br />
Kẻ ác đoạ địa ngục<br />
Người hiền lên thiên giới<br />
La Hán chứng vô sinh<br />

Không trời cao biển rộng <br />
Không hang động núi rừng<br />
Đã tạo nghiệp ác độc<br />
Trên đời hết chỗ dung<br />
 
 ***
  
   
Không trời cao biển rộng<br />
Không hang động núi rừng<br />
Đã tạo nghiệp ác độc<br />
Tử thần quyết không dung<br /> 
 ***
  
   

### Phẩm hình phạt

Hình phạt ai cũng kinh<br />
Mất mạng ai cũng khiếp<br />
Lấy ta suy ra người<br />
Chớ giết chớ bảo giết<br /> 
 ***
  
   

Hình phạt ai cũng kinh<br />
Sinh mạng ai cũng tiếc<br />
Lấy ta suy ra người<br />
Chớ giết chớ bảo giết<br /> 
 ***
  
   

Ai mưu cầu Hạnh phúc <br />
Bằng cách hại chúng sinh<br />
Các loài thích an lành<br />
Đời sau chẳng hạnh phúc<br /> 
 ***
  
   

Ai mưu cầu Hạnh phúc<br />
Bằng cách không sát sanh<br />
Các loài thích an lành<br />
Đời sau được hạnh phúc<br /> 
 ***
  
   

Chớ buông lời cay nghiệt<br />
Nhiếc người người nhiếc ta<br />
Khổ thay lời hiềm hận<br />
Xung đột mãi lại qua<br />
 
 ***
  
   
Nếu tự mình im lặng<br />
Như chuông nứt nhiều đàng<br />
Người ấy chứng niết bàn<br />
Hận thù không còn nữa<br />
 
 ***
  
   
Như gậy người chăn bò<br />
Lùa bò ra đồng cỏ<br />
Già chết cũng thế đó<br />
Xua người đến diệt vong<br />
 
 ***
  
   
Kẻ ngu gây nghiệp ác<br />
Không biết việc mình làm<br />
Tử chuốc khổ thở than<br />
Như bị lửa thiêu đốt<br />
 
 ***
  
   
Dùng hung khí trừng phạt<br />
Người Hiền thiện từ tâm<br />
Sẽ bị nghiệp hành thân<br />
Một trong mười oan nghiệt<br />

 
 ***
  
   
Hoặc khổ đau khốc liệt<br />
Hoặc tai biến tổn thân<br />
Hoặc bệnh ác vô ngần<br />
Hoặc tán thân loạn ý<br />

 
 ***
  
   

Hoặc bị vua bức bách<br />
Hoặc bị tội vu oan<br />
Hoặc quyến thuộc ly tan<br />
Hoặc gia tài đổ nát<br />
 
 ***
  
   
Hoặc nhà cửa cháy mạt<br />
hoặc mất mạng tán thân<br />
Kịp đến khi mạng chung<br />
Chắc chắn đoạ địa ngục<br />

 
 ***
  
   


Chẳng phải sống loã thể<br />
Bện tóc mình trét bùn<br />
Tuyệt thực nằm trên đất<br />
Bôi tro ngồi xổm chân<br />
Là sạch được thân tâm<br />
Nêu chưa dứt lậu hoặc<br /> 
 ***
  
   

Dù trang sức lộng lẫy<br />
Nhưng nhiếp phục an bình<br />
Tự chế tu phạm hạnh<br />
Không sát hại sanh linh<br />
Đó là Bà la môn<br />
Tỳ kheo hay khất sĩ<br /> 
 ***
  
   

Hiếm thấy ai ở đời<br />
Biết tự chế khiêm tốn<br />
Tránh mọi lời thương tổn<br />
Như ngựa hiền tránh roi<br />
 
 ***
  
   
Như ngựa hiền phải roi<br />
Hãy nhiệt tâm hăng hái<br />
Giới đức tín tinh cần<br />
Trạch pháp tu thiền định<br />
Minh hạnh tâm chánh tịnh<br />
Dứt thống khổ ưu phiền<br />
 
 ***
  
   

Người đem nước dẫn nước <br />
Người làm tên vót tên<br />
Thợ mộc uốn gỗ bền<br />
Bậc Trị tự điều phục<br />
 
 ***
  
   
## Phẩm già yếu

Hân hoan vui thú gì<br />
Khi trần gian hực lửa<br />
Tối tăm mãi vây bủa<br />
Sao không tìm ánh dương<br />
 
 ***
  
   
Nhìn hình hãi xinh đẹp<br />
Một khối nặng khổ đau<br />
Bệnh ác nhiều lo nghĩ<br />
Có gì vĩnh cữu đâu<br />
 
 ***
  
   
Thân xác này kiệt quệ<br />
Ở tật bệnh hoại hư<br />
Khỏi nhiễm ô tan rã<br />
Chết kết thúc mạng người<br />

 
 ***
  
   
Như que xương trắng đục<br />
Như trái bầu mùa thu<br />
Bị vứt nằm lăn lóc<br />
Vui sướng gì ngắm ư<br />
 
 ***
  
   
Thành này xây bằng xương<br />
Trét tô bằng thịt máu<br />
Già chết và kiêu mạn<br />
Huỷ báng chứa nơi đây<br />
 
 ***
  
   

Xe vui rồi sẽ cũ<br />
Thân xác này sẽ già<br />
Pháp thiện nhơn bất lão<br />
Do Thiện nhơn truyền ra<br />
 
 ***
  
   
Người ít nghe kém học
Phát triển như trâu bò<br />
Thịt xương ngày một to<br />
Trí tuệ chẳng tăng trưởng<br /> 
 ***
  
   

Lang thang bao kiếp sống <br />
Ta tiềm kẻ xây nhà<br />
Tìm mãi vẫn không ra<br />
Nên luân hồi đau khổ<br /> 
 ***
  
   

Hỡi kẻ làm nhà kia<br />
Ta thấy mặt người rồ<br />
Rui mè đòn dong gãy<br />
Ngươi hết làm nhà thôi<br />
Tâm ta chừ tịch tịnh<br />
Tham ái dứt bặt rồi<br /> 
 ***
  
   


Lúc trẻ không phạm hạnh<br />
Tiền của chẳng góp gom<br />
Như cò già ủ rũ<br />
Bên hồ không cá tôm<br />
 
 ***
  
   
Lúc trẻ không Phạm hạnh<br />
Tiền của không góp gom<br />
Như cánh cung mòn gãy<br />
Tham dĩ vãng chẳng còn<br />


 
 ***
  
   

### Phẩm tự ngã
 
 ***
  
   

Nếu ta yêu quý ta<br />
Phải bảo vệ tối đa<br />
Một trong ba canh ấy <br />
Người Trí phải tỉnh ra<br />
 
 ***
  
   
Người Trí trước đặt mình<br />
Vào nếp sống chánh hạnh<br />
Sau ra giáo hoá người<br />
Ắt khỏi bị khiển trách<br />

 
 ***
  
   
Hãy làm cho kì được <br />
Những điều mình dạy người<br />
Khéo nhiếp mình nhiếp người<br />
Khó thay tự điều nhiếp<br />
 
 ***
  
   
Hãy nương tựa chính mình<br />
Chơ nương tựa ai khác?<br />
Người khéo điều phục mình<br />
Đạt chỗ tựa khó đạt<br />

 
 ***
  
   

Ác nghiệp do mình gây<br />
Ác nghiệp do mình tạo<br />
Ác nghiệp nghiền kẻ ngu<br />
Như kim cương mài ngọc<br /> 
 ***
  
   

Kẻ vung tay phá giới<br />
Như cây bị dây leo<br />
Tự chuốc lấy hiểm nghèo<br />
Kẻ thù muốn như vậy<br /> 
 ***
  
   


Việc ác rất dễ làm<br />
Nhưng chẳng lợi cho ta<br />
Việc ân ích từ thiện<br />
Thật khó làm lắm đa<br />
 
 ***
  
   
Kẻ ngu ôm tà kiến<br />
Khinh miệt pháp thánh Tăng<br />
Bậc La Hán chánh hạnh<br />
Sẽ tự diệt căn lành<br />
Như trái cây lan chính<br />
Tự huỷ hoại thân nhanh<br />

 
 ***
  
   

Tự ta gây ác nghiệp<br />
Tự ta nhiễm cấu trần<br />
Tự ta tránh ác nghiệp<br />
Tự ta tịnh thân tâm<br />
Nhiễm tịnh do ta cả<br />
Không ai thanh tịnh ai<br />
 
 ***
  
   
Dù lợi người bao nhiêu<br />
Cũng đừng quên Tự lợi<br />
Hiểu rõ được tự lợi<br />
Quyết trí đạt lợi riêng<br />

 
 ***
  
   



### Phẩm thế gian
 
 ***
  
   
Chớ theo đời ti tiện<br />
Chớ nương thói buông lung<br />
Chớ vướng vúi tục trần<br />
Chớ ôm ấp tà vọng<br /> 
 ***
  
   

Tinh cần chớ phóng dật<br />
Chánh Hạnh chớ buông lung<br />
Người chuyên tâm chánh hạn<br />
Đời đời vui khôn cùng<br /> 
 ***
  
   

Hãy vui đời chánh hạnh<br />
Chớ phóng dật buông lung<br />
Người chuyên tâm chánh hạnh<br />
Đời đời vui khôn cùng<br /> 
 ***
  
   

Như bọt nước trôi sông<br />
Như huyễn hoá bềnh bồng<br />
Nếu nhìn đời như vậy<br />
Tử thần hết thấy ông<br /> 
 ***
  
   


Hãy xem thế gian này<br />
Như xe vua lộng lẫy<br />
Kẻ ngu ngắm mê mãi<br />
Người trí chẳng bận tâm<br /> 
 ***
  
   

Ai trước sống buông lung<br />
Sau tinh chuyên chánh hạnh<br />
Sẽ soi sáng nhân gian<br />
Như trăng lên mây tạnh<br /> 
 ***
  
   

Ai xua tan ác nghiệp<br />
Bằng thiện ý hạnh lành<br />
Sẽ soi sáng quần sanh<br />
Như trăng lên mây tạnh<br />
 
 ***
  
   
Thiên hạ thật mù quáng<br />
Mấy ai sáng suốt nào<br />
Như chim thoát khỏi lưới<br />
Mây con vút trời cao<br />
 
 ***
  
   



Như thiên nga giữa trời<br />
Thần thông bay khắp nơi<br />
Hàng phục ma quân hết<br />
Bậc trí siêu thoát đời<br />
 
 ***
  
   
Ai nói lời hy vọng<br />
Ai phá pháp nhất thừa<br />
Ai bác đời sau ấy<br />
Không ác nào không bừa<br />
 
 ***
  
   
Kẻ ngu ghép hào phóng<br />
Người bần chẳng sanh thiên<br />
Bậc Trí vui bố thí<br />
Đời sau hưởng phước điền<br />
 
 ***
  
   
Đắc quả Tu Đà Hoàn<br />
Hơn chinh phục nhân gian<br />
Hơn tái sinh thiên giới<br />
Hơn bá chủ trần gian<br />
 
 ***
  
   
### Phẩm Phật đà
 
 ***
  
   

Sạch dục lạc tham ái<br />
Bặt khát vọng trên đời<br />
Trí Lực Phật vô lượng<br />
Cám dỗ sao được ư<br />
 
 ***
  
   
Giải thoát mọi ràng buộc<br />
Bặt ái dụng trên đời<br />
Trí Lực Phật vô lượng<br />
Cám dỗ sao được ư<br />
 
 ***
  
   

Người Trí chuyên thiền định<br />
Thích an tịnh viễn ly<br />
Bậc chánh giác chánh niệm<br />
Chư thiên cũng kính quy<br />
 
 ***
  
   
Khó thay được làm người<br />
Khó thay sống vui tươi<br />
Khó thay nghe Diệu pháp<br />
Khó thay Phật ra đời<br />
 
 ***
  
   
Đừng làm các điều ác<br />
Tu tập mọi hạnh lành<br />
Giữ tâm ý trong sạch<br />
Đó là lời Phật dạy<br /> 
 ***
  
   

Chư Phật thường giảng dạy<br />
Nhẫn nhục hạnh tối cao<br />
Niết bàn quả vô thượng<br />
Xuất gia nhiểu hại người<br />
Đâu còn Sa môn tướng<br />
 
 ***
  
   
Chớ hãm hãi huỷ báng<br />
Giới cấu bẩn nghiêm trì<br />
Ăn uống có tiết độ<br />
An trụ nơi viễn ly<br />
Chuyên tu tập thiền định<br />
Lời chư Phật nhớ ghi<br />
 
 ***
  
   
Dầu mưa tuôn vàng bạc<br />
Dục Lạc vẫn chưa vừa<br />
Càng khoái lạc say sưa<br />
Ắt khổ nhiều ít vui<br />
 
 ***
  
   
Biết vậy nên người Trí<br />
Chẳng thích Lạc chư thiên<br />
Để tử bậc Chánh giác<br />
Quyết diệt tham ái liền<br />
 
 ***
  
   
Lắm người sợ hoảng hốt<br />
Tìm nhiều chỗ nương vào<br />
Hoặc rừng thẳm núi cao<br />
Hoặc vườn cây đền tháp<br />
 
 ***
  
   
Nương tựa vậy chưa yên<br />
Chưa tối thượng phước điền<br />
Người nương tựa như vậy<br />
Thoát sao hết ưu phiền<br /> 
 ***
  
   

Ai nương tựa theo Phật<br />
Chánh pháp và Thánh tăng<br />
Dùng chánh Kiến thấy rõ<br />
Bốn thánh đế thường hằng<br />
 
 ***
  
   
Một khổ hai nguyên nhân<br />
Ba vượt khổ xuất trần<br />
Bốn là đường tám nhánh<br />
Tận diệt khổ khổ nhân<br />
 
 ***
  
   
Nương tựa vậy là yên<br />
Là tối thượng phước điền<br />
Người nương tựa như vậy<br />
Giải thoát hết ưu phiền<br />
 
 ***
  
   

Thánh nhân rất khó gặp<br />
Vì không hiện khắp nơi<br />
Bậc Trí sanh ở đâu<br />
Gia tộc đó an lạc<br />
 
 ***
  
   
Vui thay Phật đản sinh<br />
Vui thay Pháp thuyết minh<br />
Vui thay Tăng hoà hợp<br />
Vui thay giới tu hành<br />
 
 ***
  
   

Kính lễ bậc đáng kính<br />
Chư Phật hay môn đồ<br />
Các bậc sạch chướng ngại<br />
Đoạn ưu khổ tế thô<br />
 
 ***
  
   

Công đức người kính lễ<br />
Bậc vô uý tịch tịnh<br />
Thật vô lượng vô biên<br />
Không thề nào lường định<br />
 
 ***
  
   
### Phẩm an lạc
 
 ***
  
   
Lành thay ta vui sướng<br />
Từ ái giữa oán thù<br />
Giữa những người oán thù<br />
Ta sống không thù oán<br />
 
 ***
  
   
Lành thân ta vui sống<br />
Khoẻ mạnh giữa yếu đau<br />
Giữa những người yếu đau<br />
Ta sống không đau yếu<br />
 
 ***
  
   
Lành thay ta vui sống<br />
Vô dục giữa khát khao<br />
Giữa những người khát khao<br />
Ta sống không khao khát<br />
 
 ***
  
   
Lành thay ta vui sống<br />
Không chướng ngại ngấm ngầm<br />
Tận hưởng nguồn hỷ lạc<br />
Như chư thiên Quang Âm<br />

 
 ***
  
   
Chiến thằng gây thù hận<br />
Thất bại chuốc khổ đau<br />
Từ bỏ mọi thắng bại<br />
An tịnh liền theo sau<br /> 
 ***
  
   
Lửa nào bằng lửa tham<br />
Ác nào bằng ác hận<br />
Khổ nào bằng khổ thân<br />
Vui nào bằng Tịch tịnh<br /> 
 ***
  
   

Đói bụng bệnh tối trọng<br />
Thân xác khổ vô vàn<br />
Hiểu đúng sự thật ấy<br />
Đạt vô lượng niết bàn<br /> 
 ***
  
   

Sức khoẻ là lợi ích<br />
Biết đủ là giàu sang<br />
Thành tín là họ hàng<br />
Niết bàn là hạnh phúc<br /> 
 ***
  
   

Ai nếm mùi tịch tịnh<br />
Hưởng hương vị độc cư<br />
Thoát âu lo cấu nhiễm<br />
Pháp hỷ đượm cả người<br /> 
 ***
  
   

Lành thay gặp Thánh nhân<br />
Phúc thay được kết thân<br />
Không gặp kẻ ngu muội<br />
Thực an lạc muôn phần<br />
 
 ***
  
   

Sống với kẻ si mê<br />
Ắt bốn bề sầu tủi<br />
Gần gửi người ngu muội<br />
Khổ như gần kẻ thù<br />
Thân cận bậc Trí tu<br />
Vui như gặp thân thuộc<br />
 
 ***
  
   
Nên gần bậc Hiền Trí<br />
Bậc trì giới đa văn<br />
Bậc đạo hành Thánh tăng<br />
Bậc thiện nhơn túc trí<br />
Thân cận vậy thật quý<br />
Như trăng theo đường sao<br />

 
 ***
  
   
### Phẩm Hỷ ái 
 ***
  
   

Miệt mài điều đáng tránh<br />
Buông xả việc nghiên tầm<br />
Ganh tỵ bậc chuyên tâm<br />
Bỏ đích theo Dục lạc<br /> 
 ***
  
   

Chớ gần người yêu quý<br />
Chớ thân kẻ ghét hờn<br />
Yêu không gặp héo hon<br />
Ghét phần gần đau khổ<br />
 
 ***
  
   
Yêu xa nhau là khổ<br />
Thế nên chẳng vấn vương<br />
Người dứt niệm ghét thương<br />
Là thoát vòng trói buộc<br />
 
 ***
  
   
Thân ái sinh ưu sầu<br />
Thân ái sinh sợ hãi<br />
Ai thoát khỏi thân ái<br />
Ắt hết mọi âu lo<br /> 
 ***
  
   

Hỷ ái sinh ưu sầu<br />
Hỷ ái sinh sợ hãi<br />
Ai thoát khỏi hỷ ái<br />
Ắt hết mọi âu lo<br /> 
 ***
  
   

Luyến ái sinh ưu sầu<br />
Luyến ái sinh sợ hãi<br />
Ai thoát khỏi luyến ái<br />
Ắt hết mọi âu lo<br /> 
 ***
  
   

Dục ái sinh ưu sầu<br />
Dục ái sinh sợ hãi<br />
Ai thoát khỏi dục ái<br />
Ắt hết mọi âu lo<br /> 
 ***
  
   

Tham ái sinh ưu sầu<br />
Tham ái sinh sợ hãi<br />
Ai thoát khỏi tham ái<br />
Ắt hế mọi âu  lo<br /> 
 ***
  
   

Đử giới đức chánh Kiến<br />
Liễu ngộ Pháp chân như<br />
Thành tựu mọi công hạnh<br />
Quần chúng yêu kính người<br /> 
 ***
  
   

Tu tập Pháp ly thân<br />
Tâm thành cầu thánh quả<br />
Ý dục lạc buông xả<br />
Xứng gọi bậc thượng lưu<br /> 
 ***
  
   

Bao lâu xa cố hương<br />
Ngày về được an khương<br />
Bà con và bạn hữu<br />
Mừng đón người thân thương<br /> 
 ***
  
   

Người làm Phước cũng vậy<br />
Cũng vậy được thiện nghiệp<br />
Đời này và đời sau<br />
Như bà con thân thuộc<br />
 
 ***
  
   

## Phẩm phẩn nộ
 
 ***
  
   
Diệt phẩn nộ kiêu mạn<br />
Dứt phiền não buộc ràng<br />
Đoạn chấp thủ danh sắc<br />
Khổ não hết đeo mang<br />
 
 ***
  
   
Ai dằn cơn Phẩn nộ<br />
Như hãm xe đang lăn<br />
Vị ấy đánh xe thật<br />
Người khác phụ cương phanh<br /> 
 ***
  
   

Từ bi thẳng sân hận<br />
Hiền thiện thắng hung tàn<br />
Bố thí thắng xan tham<br />
Chân thật thắng hư nguỵ<br /> 
 ***
  
   

Hãy nói lời chân thật<br />
Bố thí chớ giận hờn<br />
Làm được ba điều ấy<br />
Đạt đến cảnh thiên chơn<br />
 
 ***
  
   
Hiền sĩ không sát hại<br />
Điều phục thân mạng hoài<br />
Đạt cảnh giới bất tử<br />
Giải thoát hết bi ai<br /> 
 ***
  
   

Ai ngày đêm tu tập <br />
Chuyên tâm hướng niết bàn<br />
Thời thời thường tỉnh giác<br />
Lậu hoặc ắt tiêu tan<br /> 
 ***
  
   

Vậy đó A Tu la<br />
Xưa nay đều thế cả<br />
Ngồi im bị đả phá<br />
Nói ít bị người phê<br />
Nói nhiều bị người chê<br />
Không ai không bị trách<br />
Trên trần thế bộn bề<br /> 
 ***
  
   

Xưa vị lai và nay<br />
Đâu đó sự kiện này<br />
Kẻ hoàn toàn bị trách<br />
Người chỉ được khen hay<br /> 
 ***
  
   

Ai ngày ngày phản tỉnh<br />
Sống trong sạch đường đường<br />
Đầy đủ giới định tuệ<br />
Bậc trí thường tán dương<br /> 
 ***
  
   

Người Hạnh tợ vàng y<br />
Ai dám chê trách gì<br />
Chư thiên còn ca ngợi<br />
Phạm thiên cũng kính qui<br />
 
 ***
  
   
Giữ thân đừng nóng giận<br />
Điều phục thân an hoà<br />
Từ bỏ thân làm ác<br />
Thân chánh trực hiền hoà<br /> 
 ***
  
   

Giữ lời đừng nóng vội<br />
Điều phục lời nhu hoà<br />
Từ bỏ lời thô ác<br />
Lời từ tốn an hoà<br /> 
 ***
  
   

Giữ ý đừng nóng giận<br />
Điều phục ý khiêm hoà<br />
Từ bỏ ý độc ác<br />
Ý quảng đại bao la<br /> 
 ***
  
   

Điều phục được thân nghiệp<br />
Chế ngự được ngôn từ<br />
Thúc liễm được tâm tư<br />
Bậc Trí khéo tự chế<br />
 
 ***
  
   
### Phẩm cấu uế

Người nay như Lá héo<br />
Diêm sứ đang ngóng chờ<br />
Trước cửa chết trơ vơ<br />
Tư lương ngươi chẳng có<br /> 
 ***
  
   

Hãy tự xây hòn đảo<br />
Sáng suốt gấp tinh chuyên<br />
Trừ tham dục cấu uế<br />
Lên thánh địa chư thiên<br /> 
 ***
  
   

Đời người nay úa tàn<br />
Sắp bị Diêm sứ mang<br />
Đường trường không chỗ nghỉ<br />
Chẳng còn chú hành trang<br /> 
 ***
  
   

Hãy tự xây hòn đảo<br />
Sáng suốt gấp tinh chuyên<br />
Trừ tham dục cấu uế<br />
Dứt sanh lão ưu phiền<br />
 
 ***
  
   
Bậc Trí tẩy cấu uế<br />
Gột rửa từng sát na<br />
Như thợ bạc tinh luyện<br />
Từ từ lọc quặng ra<br />
 
 ***
  
   
Sét phát sanh từ sắt<br />
Lại ăn sắt dần dà<br />
Phạm nhân chịu đau khổ<br />
Do ác nghiệp mà ra<br />
 
 ***
  
   
Không tụng sét sách kinh<br />
Không siêng dơ cửa nhà<br />
Lười biếng bẩn thân ta<br />
Bê tha nhớp người gác<br />
 
 ***
  
   
Tà hạnh nhơ đàn bà<br />
Keo kiệt bẩn kẻ thí<br />
Ác phép vết hàn rỉ<br />
Cả đời này đời sau<br />
 
 ***
  
   
Trong các loại bẩn ấy<br />
Vô minh nhớp tột cùng<br />
Trừ cấu uế thanh tịnh<br />
Tỳ kheo ắt viên dung<br />
 
 ***
  
   
Dễ thay sống trơ tráo<br />
Lỗ mãng như quạ diều<br />
Miệng biêu rêu ngạo mạn<br />
Lòng ô nhiễm tự kiêu<br /> 
 ***
  
   

Khó thay sống khiêm tốn<br />
Thanh tịnh tâm vô tư<br />
Giản dị đời trong sạch<br />
Sáng suốt trọn kiếp người<br /> 
 ***
  
   

Ở đời ai sát sanh<br />
Láo khoét không chân thật<br />
Lừa đảo trộm tài vật<br />
Gian díu vợ người ta<br /> 
 ***
  
   

Say sưa đến sa đà<br />
Nghiện ngập suốt ngày tháng<br />
Hạnh người ấy không quản<br />
Bứng gốc mình đời nay<br /> 
 ***
  
   

Bậc Thiên nhơn nên biết<br />
Không tự chế là ác<br />
Đừng để tham phi pháp<br />
Dìm người khổ triền miên<br /> 
 ***
  
   

Do tín tâm hoan hỷ<br />
Nên người ta bố thí<br />
Ai đem lòng ganh tỵ<br />
Miếng ăn uống của người<br />
Kẻ ấy trong tâm tư<br />
Ngày đêm chẳng an tịnh<br /> 
 ***
  
   

Ai nhổ chặt gốc rễ<br />
Tận diệt thót ghét ghen<br />
Người ấy cả ngày đêm<br />
Tâm thường được an tịnh<br /> 
 ***
  
   

Lửa nào bằng tham dục<br />
Chấp nào bằng hận sân<br />
Lưới nào bằng si ám<br />
Sông nào bằng ái ân<br /> 
 ***
  
   

Lỗi người thật dễ thấy<br />
Lỗi mình khó thấy thay<br />
Lỗi người thì cố bới<br />
Lỗi mình thì cố dấu<br />
Như bầy chim núp ngay<br /> 
 ***
  
   

Nhìn thấy lỗi của người<br />
Minh sanh tâm tức giận<br />
Thế là Phiền não tăng<br />
Lậu hoặc khó diệt tận<br /> 
 ***
  
   

Hư không không dấu vết<br />
Ngoại đạo không Sa môn<br />
Nhân loại thích chướng ngại<br />
Như Lai thoát chướng phiền<br /> 
 ***
  
   

Hư không không dấu vết<br />
Ngoại đạo không Sa môn<br />
Năm uẩn không vĩnh cửu<br />
Chư Phật không động sờn<br /> 
 ***
  
   

### Phẩm pháp trụ 
 ***
  
   

Bậc Trí hướng dẫn người<br />
Vô tư và đúng pháp<br />
Người bảo vệ pháp luật<br />
Hẳn tôn trọng pháp luật<br /> 
 ***
  
   

Không phải vì nói nhiều<br />
Là xứng đáng Bậc Trí<br />
Người an tâm vô uý<br />
Thân thiện là hiện tài<br /> 
 ***
  
   

Không phải vì nói nhiều<br />
Là thọ trì Chánh Pháp<br />
Người nghe ít diệu pháp<br />
Nhưng trực nhận viên dung<br />
Chánh pháp không buông lung<br />
Là thọ trì chánh pháp<br /> 
 ***
  
   

Không phải vì Bạc đầu<br />
Mà xứng danh Trưởng lão<br />
Vị ấy dù tuổi cao<br />
Nhưng là sư già hão<br /> 
 ***
  
   

Sống chân thật chánh hạnh<br />
Vô hại điều phục mình<br />
Bậc trí trừ cấu uế<br />
Là trưởng lão cao minh<br />
 
 ***
  
   
Không phài tài hùng biện<br />
Hay vóc dáng đường đường<br />
Là ra bậc hiền lương<br />
Nếu ganh tham dối trá<br />
 
 ***
  
   
Nhổ chặt sạch gốc rễ<br />
Dập tắt tâm tham lường<br />
Bậc trí diệt sân hận<br />
Là xứng danh Hiền lương<br /> 
 ***
  
   

Không phải đầu cạo nhẵn<br />
là nên danh Sa môn<br />
Nếu buông luông láo khoét<br />
Đầy tham dục Tâm hồn<br /> 
 ***
  
   

Ai hàng phục trọn vẹn<br />
Mọi ác nghiệp tế thô<br />
Vị ấy là Sa môn<br />
Nhờ trừ nghiệp thê tế<br /> 
 ***
  
   

Không phải đi khất thực<br />
Là đích thực Tỳ kheo<br />
Bậc đích thực Tỳ khoe<br />
Là sống theo Giới luật<br /> 
 ***
  
   

Ai siêu việc thiện ác<br />
Sống đức hạnh tuyệt vời<br />
Thấu triệt được lẽ đời<br />
Là Tỳ kheo đích thực<br /> 
 ***
  
   

Im lặng nhưng ngu si <br />
Đâu phải là Hiền trí<br />
Như cầm công công lý<br />
Bậc Trí chọn điều lành<br />
 
 ***
  
   

Từ bỏ mọi ác pháp<br />
Là xứng danh bậc trí<br />
Người được gọi Hiền sĩ<br />
Am hiểu cả hai đời<br />
 
 ***
  
   
Còn sát hại chúng sanh<br />
Đâu phải là hiền thánh<br />
Không sát hại chúng sanh<br />
Là đích thị Thánh hiền<br /> 
 ***
  
   

Không phải giữ giới luật<br />
Khổ hạnh hay học nhiều<br />
Thiền địch hay ẩn dật<br />
Mà sanh tâm Tự kiêu<br /> 
 ***
  
   

Ta hưởng phúc xuất thế<br />
Phàm phu hưởng được nào<br />
Tỳ khoe chớ tự mãn<br />
Lậu hoặc hãy triệt tiêu<br /> 
 ***
  
   

### Phẩm chánh đạo
 
 ***
  
   
Tám nhánh đường thù thắng<br />
Bốn câu Lý tuyệt luân<br />
Ly tham Pháp tối thượng<br />
Pháp nhãn đấng siêu quần<br /> 
 ***
  
   

Hướng tri kiến thanh tịnh<br />
Duy chỉ có đường này<br />
Nếu ngươi theo đường này<br />
Ma vương ắt rối loạn<br />
 
 ***
  
   
Đi trên đường Tám nhánh<br />
Là tránh mọi đau thương<br />
Ta dạy ngươi con đường<br />
Nhổ sạch mọi gai gốc<br />
 
 ***
  
   
Hãy nổ lực Tinh tấn<br />
Như Lai bậc dẫn đường<br />
Ai tu tập Thiền định<br />
Ắt thoát vòng ma vương<br />
 
 ***
  
   
Nhờ Trí tuệ quán chiếu<br />
Thấy các hành vô thường<br />
Thế là chán đau thương<br />
Đây chính đường thanh tịnh<br /> 
 ***
  
   

Nhờ trí tuệ quán chiếu<br />
Thấy các hành khổ đau<br />
Thế là chán khổ đau<br />
Đây chính đường thanh tịnh<br /> 
 ***
  
   

Nhờ trí tuệ quán chiếu<br />
Thấy Pháp vô ngã rồi<br />
Thế là chán khổ thôi<br />
Đây chính đường thanh tịnh<br /> 
 ***
  
   

Khi không nổ lực<br />
Tuy trẻ khoẻ nhưng lười<br />
Chí cùn Trí thụ động <br />
Ngộ đạo sao được ngươi<br /> 
 ***
  
   

Thân không được làm ác<br />
Khéo giữ ý giữ lời<br />
Thường thanh tịnh Ba nghiệp<br />
Đạt đạo thánh nhân thôi<br /> 
 ***
  
   

Tu thiền trí tuệ sanh<br />
Bỏ thiền trí tuệ diệt<br />
Được mất khéo phân biệt<br />
Biết rõ đường chánh tà<br />
Tự nổ lực theo đà<br />
Trí tuệ dần tăng trưởng<br /> 
 ***
  
   

Đốn rừng chớ đốn cây<br />
Vì rừng gây sợ hãi<br />
Nên đốn rừng tham ái<br />
Tỳ kheo hãy ly tham<br /> 
 ***
  
   

Bao lâu chưa đoạn tuyệt<br />
Ái dục giữa gái trai<br />
Tâm tư theo đuổi hoài<br />
Như Bê con theo mẹ<br /> 
 ***
  
   

Hãy cắt diệt ái dục<br />
Như tay ngắt sen thu<br />
Đạo tịch tịnh gắng tu<br />
Bậc Thiệt thệ dạy vậy<br /> 
 ***
  
   

Mùa mưa ta ở đây<br />
Hè thu ta ở đây<br />
Kẻ ngu si nghĩ vậy<br />
Nào đâu thấy hiểm nguy<br /> 
 ***
  
   

Người ham nhiều con cái<br />
Thích súc vật dư thừa<br />
Tử thần sẽ kéo bừa<br />
Như lụt cuốn làng ngư<br /> 
 ***
  
   

Con cái nào chở che<br />
Mẹ cha nào o bế<br />
Thân thích nào bào vệ<br />
Khi bị thần chết lôi<br /> 
 ***
  
   


### Phẩm tạp lục
 
 ***
  
   
Nếu bỏ hạnh phúc nhỏ<br />
Để được hạnh phúc to<br />
Bậc trí chẳng đắn đo<br />
Bỏ ngay hạnh phúc nhỏ<br />
 
 ***
  
   
Mình mưu cầu hạnh phúc<br />
Lại gây khổ cho người<br />
Thế là chuốc hận thù<br />
Không sao trừ hết hận<br />
 
 ***
  
   

Việc đáng làm không làm<br />
Việc không đáng lại làm<br />
Kẻ phóng dật ngạo mạn<br />
Lậu hoặc dần dần lan<br />
 
 ***
  
   
Người tinh chuyên cần nắm<br />
Tu tập pháp niệm thân<br />
không làm việc không đáng<br />
Thực hành pháp chánh cần<br />
Tâm thường niệm tỉnh giác<br />
Lậu hoặc Tiêu tan dần<br />
 
 ***
  
   
Giết cả mẹ lẫn cha<br />
Hạ hai vua Đế lỵ<br />
Diệt quê hương quốc sĩ<br />
Phạm thiên đạt vô ưu<br />
 
 ***
  
   
Giết cả mẹ lẫn cha<br />
Hạ hai vui Phạm Chí<br />
Diệt luôn tướng Tài Trí<br />
Phạm Thiên đạt vô ưu<br />
 
 ***
  
   
Tư thân luôn tỉnh giác<br />
Để tử Gô ta ma<br />
Bất luận ngày hay đêm<br />
Chuyên tâm niệm Phật đà<br />
 
 ***
  
   
Tự thân luôn tỉnh giác<br />
Đệ tử Gô ta ma<br />
Bất luận ngày hay đêm<br />
Chuyên tâm niệm Đạt ma<br />
 
 ***
  
   
Tự thân luôn tỉnh giác <br />
Đệ tử Gô ta ma<br />
Bất luận ngày hay đem<br />
Chuyên tâm niệm Tăng già<br />
 
 ***
  
   
Tự thân luôn tỉnh giác<br />
Đệ tử Gô ta ma<br />
Bất luận ngày hay đêm<br />
Chuyên tâm niệm thân xác ta<br />
 
 ***
  
   
Tự thân luôn tỉnh giác<br />
Đệ tử Gô ta ma<br />
Bất luận ngày hay đêm<br />
Tâm vô hại hiền hoà<br />
 
 ***
  
   
Tự thân luôn tỉnh giác<br />
Đệ tử Gô ta ma<br />
Bất luận ngày hay đêm<br />
Vui trong cảnh thiền na<br />
 
 ***
  
   
Khó thay vui xuất gia<br />
Khổ thay sống tại gia<br />
Khổ thay bạn không hợp<br />
Khổ thay khách ta bà<br />
Thế nên đừng phiêu bạt<br />
Dừng đeo đuổi khổ đau<br />
 
 ***
  
   
Đủ giới hạnh chánh tín<br />
Nhiều tài sản danh cao<br />
Dù đi đến nơi nào<br />
Cũng được người cung kính<br />
 
 ***
  
   
Người Hiền dù ở xa<br />
Hiện ra như núi tuyết<br />
Kẻ ác dù đứng gần<br />
Như tên bắn trong đêm<br />
 
 ***
  
   
Ai ngồi nằm đơn độc<br />
Tinh tấn đi một mình<br />
Điều phục được chính mình<br />
Sẽ vui nơi rừng thẳm<br />
 
 ***
  
   

## Phẩm địa ngục 
 ***
  
   

Người mồm miệng láo khoét<br />
Kẻ làm rồi nói không<br />
Cả hai chết tương đồng<br />
Đê tiện đoạ địa ngục<br /> 
 ***
  
   

Nhiều người khoác Cà sa<br />
Ác hạnh không điều phục<br />
Kẻ ác do nghiệp thúc<br />
Phải đoạ Địa ngục thôi<br /> 
 ***
  
   

Thà nuốt hòn sắt nóng<br />
Như ngọc lửa hừng hừng<br />
Hơn phá giới buông lung<br />
Sống nhờ cơm Tín trí<br /> 
 ***
  
   

Bốn tai hoạ ấp đến<br />
Hành hạ kẻ Ngoại tình<br />
Mang tiếng ngủ không an<br />
Bị chê đoạ địa ngục<br /> 
 ***
  
   

Mang tiếng đoạ ác thú<br />
Lo sợ tâm ít vui<br />
Quốc vương phạt trọng tội<br />
Kẻ gian díu vợ người<br />
 
 ***
  
   
Vụng nắm cỏ Ku xa<br />
Là tay tay bị cắt<br />
Hạnh Sa môn tà hoặc<br />
Tất đoạ Địa ngục thôi<br />
 
 ***
  
   
Sống buông lung phóng dật<br />
Chạy theo thói nhiễm ô<br />
Hoài nghi đời phạm hạnh<br />
Thành quả đạt chi mô<br />

Nếu việc cần phải làm<br />
Hãy quyết là hết sức<br />
Thiếu công phu nghị lực<br />
Chỉ tung cát bụi mù<br /> 
 ***
  
   

Chớ phạm phải Điều ác<br />
Làm ác khổ vô vàn<br />
Việc thiện nên chu toàn<br />
Làm xong khỏi ân hận<br />
 
 ***
  
   
Như Thành sát biên thuỳ<br />
Trong ngoài canh nghiêm mật<br />
Hãy phòng hộ chính mình<br />
Đừng để cơ  hội mất<br />
Ai để cơ hội mất <br />
Đoạ địa ngục khổ đau<br />
 
 ***
  
   

Việc đáng Hổ không hổ<br />
Việc không đáng lại hổ<br />
Do ôm ấp Tà kiến<br />
Chúng sanh bị khốn khổ<br />
 
 ***
  
   
Việc đáng kính không kính<br />
Việc không đáng lại kính<br />
Do ôm ấp Tà kiến<br />
Chúng sanh bị ngục hình<br />
 
 ***
  
   
Không lỗi lại tưởng có<br />
Có lỗi lại thấy không<br />
Do ôm ấp tà kiến<br />
Chúng sanh khổ vô ngần<br />
 
 ***
  
   
Có lỗi biết rằng có<br />
Không lỗi biết rằng không<br />
Nhớ hàm dưỡng chánh Kiến<br />
Chúng sanh sướng vô ngần<br />

 
 ***
  
   

### Phẩm Voi rừng
 
 ***
  
   
Như voi giữa trận địa<br />
Kháng cự moi cung tên<br />
Ta chịu đựng huỷ báng<br />
Phá giới biết bao người<br />
 
 ***
  
   
Kẻ luyện Voi dự hội<br />
Người luyện ngựa dâng vua<br />
Bậc Tôi luyện thượng thừa<br />
Chịu đựng mọi huỷ báng<br />
 
 ***
  
   
Quý thay lửa thuần thục<br />
Quý thay giống ngựa sinh<br />
Quý thay voi ngà báu<br />
Tuyệt thay bậc luyện mình<br />
 
 ***
  
   
Chẳng phải nhờ voi ngựa<br />
Đưa ta đến Niết bàn<br />
Chính bậc tự điều phục<br />
Đạt đến bờ thênh thang<br />
 
 ***
  
   
Voi kia tên tài hộ<br />
Phát dục tiết mùi hăng<br />
Bất trị bị giam giữ<br />
Bỏ ăn nhớ rừng xanh<br />
 
 ***
  
   
Kẻ ngu si ám độn<br />
Ham ăn ngủ như heo<br />
Bạ đâu nằm lăn đó<br />
Luân hồi mãi cuốn theo<br />
 
 ***
  
   
Xưa tâm này phóng đãng<br />
Theo dục lạc đua đòi<br />
Nay chuyên tâm nhiếp phục<br />
Như quản tượng điều voi<br />
 
 ***
  
   
Hãy tinh cần vui vẻ<br />
Khéo giữ Tâm ý thầy<br />
Tự thoát khỏi ác đạo<br />
Như voi vượt sình lầy<br />
 
 ***
  
   
Thà sống cảnh cô đơn<br />
Hơn bạn bè kẻ ngốc<br />
Sống lẻ loi đơn độc<br />
Không gây nghiệp ác hành<br />
Như voi giữa rừng xanh<br />
Thênh thang vô tư lự<br />
 
 ***
  
   
Vui thay được bạn giúp<br />
Vui thay sống tri túc<br />
Vui thay chết phước duyên<br />
Vui thay hết khổ nhục<br />
 
 ***
  
   
Vui thay hầu Mẹ sanh<br />
Vui thay hầu Cha lành<br />
Vui thay hầu hiền thánh<br />
Vui thay hầu Sa môn<br />
 
 ***
  
   
Vui thay già đức hạnh<br />
VUi thay tâm tín thành<br />
Vui thay ác không tạo<br />
Vui thay Tuệ viên thành<br />
 
 ***
  
   
### Phẩm Tham ái
 
 ***
  
   
Kẻ buông lung phóng dật<br />
Tham ái tợ dây leo<br />
Đời đời vọt nhảy theo<br />
Như khỉ chuyên hái trái<br />
 
 ***
  
   
Ai sinh sống trên đời<br />
Bị ái dục lôi cuốn<br />
Khổ đau mãi tăng trưởng<br />
Như cỏ Bi gặp mưa<br />
 
 ***
  
   
Ai sinh sống trên đời<br />
Hàng phục được Tham ái<br />
Khổ đau sẽ vuột khỏi<br />
Như nước trượt lá sen<br />
 
 ***
  
   
Các ngươi hợp nhau đây<br />
Ta có lời dạy này<br />
Hãy bớt gốc tham ái<br />
Như đào rễ cả Bi<br />
Đừng để ma vương hại<br />
Như lau bị lụt đầy<br />
 
 ***
  
   
Đốn cây không đào gốc<br />
chồi tược sẽ lên hoài <br />
Tham ái chưa nhổ rễ<br />
Khổ đau mãi dằng dai<br />
 
 ***
  
   
Ba mươi sáu dòng ái<br />
Tuôn chảy theo dục trần<br />
Ý tham dục cuồn cuộn<br />
Cuốn phăng kẻ mê đần<br />
 
 ***
  
   
Dòng ái dục chảy khắp<br />
Như dây leo mọc tràn<br />
Thấy dây leo vừa lan<br />
Liền dùng Tuệ đốn gốc<br />
 
 ***
  
   
Kẻ đam mê ái dục<br />
Say đắm thao lục trần<br />
Tuy mong cầu an lạc<br />
Sanh tử vẫn hoại thân<br />
 
 ***
  
   
Người bị ái buộc ràng<br />
Hẳn lo sợ hoang mang<br />
Như Thỏ bị trói buộc<br />
Đau khổ mãi cưu mang<br />
 
 ***
  
   
Người bị ái buộc ràng<br />
Như thỏ bị trói ngang<br />
Tỳ kheo cầu niết bàn<br />
Phải dứt trừ tham dục<br />
 
 ***
  
   
Cắt ái đi xuất gia<br />
Khổ hạnh trong rừng già<br />
Đã giải thoát dục vọng<br />
Nhưng lại trở về nhà<br />
Kìa xem hạng người ấy<br />
Mở rồi buộc lại ta!<br />
 
 ***
  
   
Bậc Trí giảng dạy rằng<br />
Dây đay gai gỗ sắt<br />
Chưa phải loại buộc chặt<br />
Ham châu báu vợ con<br />
Mê trang sức phấn son<br />
Thứ đó buộc chắc nhất<br />
 
 ***
  
   
Bậc trí giảng dạy rằng<br />
Trói buộc đó rất chắc<br />
Trì kéo xuống thật chặt<br />
Khó tháo gỡ vô vàn<br />
Bậc Trí nên cắt ngang<br />
Từ khước mọi tham ái<br />
 
 ***
  
   
Người đắm say ái dục<br />
Là tự lao xuống dòng<br />
Như nhện sa vào lưới<br />
Do chính nó làm xong<br />
Bậc Trí dứt tham ái<br />
Ắt thoát khổ thong dong<br />
 
 ***
  
   

Bỏ quá hiện vị lai<br />
Tâm ý thoát ai hoài<br />
Vượt sang bờ hiện hữu<br />
Dứt sanh lão bi ai<br />
 
 ***
  
   
Kẻ vọng tâm tà ý<br />
Say đắm theo dục trần<br />
Tham ái ngày tăng trưởng<br />
Tự làm dây buộc thân<br />
 
 ***
  
   
Người thích trừ tà ý<br />
Quán bất tịnh niệm thường<br />
Sẽ đoạn diệt tham ái<br />
Cắt đứt vòng ma vương<br />
 
 ***
  
   
Đến đích hết sợ hãi<br />
Ly ái tham tiêu tùng<br />
Cắt tiệt gai sinh tử<br />
Thân này thân cuối cùng<br />
 
 ***
  
   
Đoạn ái dục chấp thủ<br />
Khéo giải tù ngữ nguyên<br />
Thấu triệt dạng cú pháp<br />
Phối hợp chứng liền liền<br />
Mang sắc thân lần cuối<br />
Bậc đại nhân thâm ngôn<br />
 
 ***
  
   
Ta hàng phục tất cả<br />
Ta hiểu rõ ngọn ngành<br />
Ta giữ sạch cách pháp<br />
Ta đoạn tuyệt mối manh<br />
Ta diệt ái giải thoát<br />
Ta liễu ngộ viên thành<br />
Ai là Thầy ta nữa?<br />
 
 ***
  
   
Thí nào bằng Pháp thí<br />
Vị nào bằng Pháp vị<br />
Hỷ nào bằng Pháp Hỷ<br />
Diệt ái hết khổ luỵ<br />
 
 ***
  
   
Của cải hại kẻ ngu<br />
Không tìm người Trí giác<br />
Kẻ ngu ham tiền bạc<br />
Tự hại mình hại người<br />

 
 ***
  
   






https://youtu.be/5TqbR1NlqXo?t=5718





































































